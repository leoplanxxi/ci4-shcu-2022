-- MariaDB dump 10.19  Distrib 10.6.5-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: shcu_repo_2022
-- ------------------------------------------------------
-- Server version	10.6.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `shcu_repo_2022`
--

/*!40000 DROP DATABASE IF EXISTS `shcu_repo_2022`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `shcu_repo_2022` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;

USE `shcu_repo_2022`;

--
-- Table structure for table `acuse`
--

DROP TABLE IF EXISTS `acuse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acuse` (
  `id_acuse` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_acuse`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acuse`
--

LOCK TABLES `acuse` WRITE;
/*!40000 ALTER TABLE `acuse` DISABLE KEYS */;
INSERT INTO `acuse` VALUES (1,'Ninguno',1),(2,'Rectoría',1),(3,'Secretaría General',1),(4,'Secretaría Técnica',1),(5,'Relaciones Internacionales',1);
/*!40000 ALTER TABLE `acuse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archivo_material_sesion`
--

DROP TABLE IF EXISTS `archivo_material_sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivo_material_sesion` (
  `id_archivo_material_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `descripcion` varchar(1000) NOT NULL,
  `archivo` varchar(1000) NOT NULL,
  `fecha_subida` datetime NOT NULL DEFAULT current_timestamp(),
  `activo` int(11) NOT NULL DEFAULT 1,
  `sesion` int(11) NOT NULL,
  PRIMARY KEY (`id_archivo_material_sesion`),
  KEY `sesion` (`sesion`),
  CONSTRAINT `archivo_material_sesion_ibfk_1` FOREIGN KEY (`sesion`) REFERENCES `sesion` (`id_sesion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivo_material_sesion`
--

LOCK TABLES `archivo_material_sesion` WRITE;
/*!40000 ALTER TABLE `archivo_material_sesion` DISABLE KEYS */;
INSERT INTO `archivo_material_sesion` VALUES (1,'Archivo de sesión','Orden del día','/public/uploads/material_sesiones/4/fw8ben.pdf','2022-01-07 12:30:44',1,4),(2,'Archivo de sesión','Orden del día parte 2','/public/uploads/material_sesiones/4/authorization_form (3).pdf','2022-01-09 16:14:33',1,4),(3,'Prueba','Orden del día','/public/uploads/material_sesiones/7/W9_INSTRUCTION.pdf','2022-01-09 18:11:01',1,7);
/*!40000 ALTER TABLE `archivo_material_sesion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `id_asistencia` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_sesion` int(11) NOT NULL,
  `asistencia` int(11) NOT NULL DEFAULT 1,
  `activo` int(11) NOT NULL DEFAULT 1,
  `voto` int(11) NOT NULL DEFAULT 1,
  `propietario` int(11) NOT NULL DEFAULT 1,
  `id_usuario_suplente` int(11) NOT NULL,
  `justificante` varchar(500) NOT NULL,
  `descripcion_justificante` varchar(500) NOT NULL,
  PRIMARY KEY (`id_asistencia`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_sesion` (`id_sesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencia`
--

LOCK TABLES `asistencia` WRITE;
/*!40000 ALTER TABLE `asistencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `asistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitacora_descargas`
--

DROP TABLE IF EXISTS `bitacora_descargas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora_descargas` (
  `id_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_consejeros` int(11) NOT NULL,
  `id_material_sesion` int(11) NOT NULL,
  `fecha_descarga` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_bitacora`),
  KEY `id_usuario_consejeros` (`id_usuario_consejeros`),
  KEY `id_material_sesion` (`id_material_sesion`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora_descargas`
--

LOCK TABLES `bitacora_descargas` WRITE;
/*!40000 ALTER TABLE `bitacora_descargas` DISABLE KEYS */;
INSERT INTO `bitacora_descargas` VALUES (1,1,11,'2022-01-02 13:28:05'),(2,1,1,'2022-01-07 12:30:57'),(3,1,1,'2022-01-07 12:41:41'),(4,1,1,'2022-01-07 13:01:22'),(5,1,1,'2022-01-07 13:19:46'),(6,1,1,'2022-01-07 13:19:58'),(7,1,1,'2022-01-09 16:13:20'),(8,1,2,'2022-01-09 16:14:53'),(9,1,3,'2022-01-09 18:11:22'),(10,1,3,'2022-01-09 18:11:43'),(11,2,3,'2022-01-09 18:15:37'),(12,1,3,'2022-01-10 12:40:45');
/*!40000 ALTER TABLE `bitacora_descargas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo_consejero`
--

DROP TABLE IF EXISTS `cargo_consejero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo_consejero` (
  `id_cargo_consejero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_cargo_consejero`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo_consejero`
--

LOCK TABLES `cargo_consejero` WRITE;
/*!40000 ALTER TABLE `cargo_consejero` DISABLE KEYS */;
INSERT INTO `cargo_consejero` VALUES (1,'Director',1),(2,'Docente',1),(3,'Alumno',1),(4,'Adjunto',1);
/*!40000 ALTER TABLE `cargo_consejero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comisiones`
--

DROP TABLE IF EXISTS `comisiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comisiones` (
  `id_comisiones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_comisiones`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comisiones`
--

LOCK TABLES `comisiones` WRITE;
/*!40000 ALTER TABLE `comisiones` DISABLE KEYS */;
INSERT INTO `comisiones` VALUES (1,'Ninguna',1),(2,'Honor y Justicia',1),(3,'Legislación Universitaria',1),(4,'Grados Honoríficos y Distinciones',1),(5,'Presupuesto',1),(6,'Glosa',1),(7,'Patrimonio',1),(8,'Supervisión Administrativa',1),(9,'Planeación',1),(10,'Obras y Crecimiento Físico',1),(11,'Protección y Preservación de Patrimonio Cultural',1),(12,'Comisión Especial de Género',1);
/*!40000 ALTER TABLE `comisiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comisiones_consejeros`
--

DROP TABLE IF EXISTS `comisiones_consejeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comisiones_consejeros` (
  `id_consejero` varchar(20) NOT NULL,
  `id_comision` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  KEY `id_consejero` (`id_consejero`),
  KEY `id_comision` (`id_comision`),
  CONSTRAINT `comisiones_consejeros_ibfk_1` FOREIGN KEY (`id_comision`) REFERENCES `comisiones` (`id_comisiones`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comisiones_consejeros_ibfk_2` FOREIGN KEY (`id_consejero`) REFERENCES `usuarios_consejeros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comisiones_consejeros`
--

LOCK TABLES `comisiones_consejeros` WRITE;
/*!40000 ALTER TABLE `comisiones_consejeros` DISABLE KEYS */;
/*!40000 ALTER TABLE `comisiones_consejeros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daua`
--

DROP TABLE IF EXISTS `daua`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daua` (
  `id_daua` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_daua`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daua`
--

LOCK TABLES `daua` WRITE;
/*!40000 ALTER TABLE `daua` DISABLE KEYS */;
INSERT INTO `daua` VALUES (1,'Facultad de Ciencias de la Computación',1),(2,'FACULTAD DE ADMINISTRACIÓN',1),(3,'FACULTAD DE ARQUITECTURA',1),(4,'ESCUELA DE ARTES',1),(5,'ESCUELA DE BIOLOGÍA',1),(6,'FACULTAD DE CIENCIAS DE LA COMUNICACIÓN',1),(7,'FACULTAD DE CONTADURÍA PÚBLICA',1),(8,'FACULTAD DE CIENCIAS DE LA ELECTRÓNICA',1),(9,'FACULTAD DE FÍSICO MATEMÁTICAS',1),(10,'FACULTAD DE CIENCIAS QUÍMICAS',1),(11,'INSTITUTO DE CIENCIAS',1),(12,'INSTITUTO DE CIENCIAS DE GOBIERNO Y DESARROLL',1),(13,'INSTITUTO DE CIENCIAS SOCIALES Y HUMANIDADES ',1),(14,'INSTITUTO DE FÍSICA \"ING. LUIS RIVERA TERRAZA',1),(15,'INSTITUTO DE FISIOLOGÍA',1),(16,'FACULTAD DE CULTURA FÍSICA',1),(17,'FACULTAD DE DERECHO Y CIENCIAS SOCIALES',1),(18,'FACULTAD DE ECONOMIA',1),(19,'FACULTAD DE ENFERMERÍA',1),(20,'FACULTAD DE ESTOMATOLOGÍA',1),(21,'FACULTAD DE FILOSOFÍA Y LETRAS',1),(22,'FACULTAD DE INGENIERÍA',1),(23,'FACULTAD DE INGENIERÍA AGROHIDRÁULICA',1),(24,'FACULTAD DE INGENIERÍA QUÍMICA',1),(25,'FACULTAD DE LENGUAS',1),(26,'FACULTAD DE MEDICINA',1),(27,'FACULTAD DE MEDICINA VETERINARIA Y ZOOTECNIA',1),(28,'FACULTAD DE PSICOLOGÍA',1),(29,'BACHILLERATO \"5 DE MAYO\"',1),(30,'PREPARATORIA \"2 DE OCTUBRE DE 1968\"',1),(31,'PREPARATORIA \"ALFONSO CALDERÓN MORENO\"',1),(32,'PREPARATORIA \"GRAL. EMILIANO ZAPATA\"',1),(33,'PREPARATORIA \"GRAL. LÁZARO CÁRDENAS DEL RÍO\"',1),(34,'PREPARATORIA \"LIC. BENITO JUÁREZ GARCÍA\"',1),(35,'PREPARATORIA REGIONAL \"ENRIQUE CABRERA BARROS',1),(36,'PREPARATORIA REGIONAL \"SIMÓN BOLÍVAR\"',1),(37,'PREPARATORIA URBANA \"ENRIQUE CABRERA BARROSO\"',1),(38,'ESCUELA DE ARTES PLÁSTICAS AUDIOVISUALES',1),(39,'AREA CENTRO (CAROLINO)',1),(40,'AREA CU',1),(41,'AREA SALUD',1);
/*!40000 ALTER TABLE `daua` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacion_consejero`
--

DROP TABLE IF EXISTS `informacion_consejero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacion_consejero` (
  `id` varchar(20) NOT NULL,
  `grado` varchar(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `cumpleanos` date NOT NULL,
  `cargo` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `daua` varchar(100) NOT NULL,
  `tel_cel_1` varchar(20) NOT NULL,
  `tel_cel_1_whats` tinyint(1) NOT NULL DEFAULT 0,
  `tel_cel_2` varchar(20) DEFAULT NULL,
  `tel_cel_2_whats` tinyint(1) DEFAULT 0,
  `tel_casa` varchar(20) NOT NULL,
  `tel_oficina` varchar(20) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `correo_institucional` varchar(250) NOT NULL,
  `correo_personal` varchar(250) NOT NULL,
  `comision` int(11) DEFAULT 1,
  `protesta` date NOT NULL,
  `consejero_asociado` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `foto` varchar(250) NOT NULL,
  KEY `id` (`id`),
  KEY `cargo` (`cargo`),
  KEY `tipo` (`tipo`),
  CONSTRAINT `informacion_consejero_ibfk_1` FOREIGN KEY (`id`) REFERENCES `usuarios_consejeros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `informacion_consejero_ibfk_2` FOREIGN KEY (`cargo`) REFERENCES `cargo_consejero` (`id_cargo_consejero`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `informacion_consejero_ibfk_3` FOREIGN KEY (`tipo`) REFERENCES `tipo_consejero` (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacion_consejero`
--

LOCK TABLES `informacion_consejero` WRITE;
/*!40000 ALTER TABLE `informacion_consejero` DISABLE KEYS */;
INSERT INTO `informacion_consejero` VALUES ('200914922','M. en C.','Alvaro Osvaldo','Lopez','Garcia','1991-11-17',3,1,'1','2223193466',1,'',0,'2222191549','','','','alvaro.lopez@correo.buap.mx',9,'2022-01-17',201040900,1,''),('201040900','M. en C.','Pedro','Paramo','Perez','1992-10-11',3,2,'1','2461113814',1,'2228904455',1,'2222444727','2222295500','1129','pedro.paramo@correo.buap.mx','pedrito@hotmail.com',4,'2022-01-02',200914922,1,''),('201214833','Lic.','Giovana','Mendez','Gomez','1994-03-22',3,1,'1','',0,NULL,0,'',NULL,NULL,'','',1,'2022-01-03',201535852,1,''),('201535852','Lic.','Mariana','Campos','Meza','1996-06-02',3,2,'1','',0,NULL,0,'',NULL,NULL,'','',1,'2022-01-03',201214833,1,''),('200626887','','Felipe','Sanchez','Ritz','1981-04-06',3,1,'1','',0,NULL,0,'',NULL,NULL,'','',1,'2022-01-10',200914922,1,'');
/*!40000 ALTER TABLE `informacion_consejero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misc`
--

DROP TABLE IF EXISTS `misc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misc` (
  `id_misc` int(11) NOT NULL AUTO_INCREMENT,
  `llave` varchar(200) NOT NULL,
  `valor` longtext NOT NULL,
  PRIMARY KEY (`id_misc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misc`
--

LOCK TABLES `misc` WRITE;
/*!40000 ALTER TABLE `misc` DISABLE KEYS */;
INSERT INTO `misc` VALUES (1,'aviso_sesion','Primera sesión ordinaria<br />\r\n17 de enero de 2022<br />\r\n'),(2,'disco_sesion',''),(3,'sesion_activa','7');
/*!40000 ALTER TABLE `misc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador',1),(2,'Enlace',1),(3,'Secretario Técnico',1),(4,'Unidad Académica',1),(5,'Consejero',1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sesion`
--

DROP TABLE IF EXISTS `sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sesion` (
  `id_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `fecha` date NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `asistencia` longtext NOT NULL,
  PRIMARY KEY (`id_sesion`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sesion`
--

LOCK TABLES `sesion` WRITE;
/*!40000 ALTER TABLE `sesion` DISABLE KEYS */;
INSERT INTO `sesion` VALUES (4,'Primera sesión ordinaria','2022-01-17',0,'{\"1\":{\"200914922\":{\"asistencia\":\"1\",\"propietario\":1,\"resp\":1,\"voto\":\"2\",\"justificante\":\"\"},\"201040900\":{\"asistencia\":0,\"propietario\":0,\"resp\":0,\"voto\":1,\"justificante\":\"\"},\"201214833\":{\"asistencia\":\"1\",\"propietario\":1,\"resp\":1,\"voto\":\"1\",\"justificante\":\"\"},\"201535852\":{\"asistencia\":0,\"propietario\":0,\"resp\":0,\"voto\":1,\"justificante\":\"\"}}}'),(7,'Primera sesión ordinaria','2022-02-20',1,'{\"1\":{\"200914922\":{\"asistencia\":\"0\",\"propietario\":1,\"resp\":1,\"voto\":\"3\",\"justificante\":\"\"},\"201040900\":{\"asistencia\":\"1\",\"propietario\":0,\"resp\":0,\"voto\":\"3\",\"justificante\":\"\"},\"201214833\":{\"asistencia\":\"1\",\"propietario\":1,\"resp\":0,\"voto\":\"1\",\"justificante\":\"\"},\"201535852\":{\"asistencia\":0,\"propietario\":0,\"resp\":1,\"voto\":1,\"justificante\":\"\"}}}');
/*!40000 ALTER TABLE `sesion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitudes`
--

DROP TABLE IF EXISTS `solicitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitudes` (
  `id_solicitud` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(1500) NOT NULL,
  `fecha` date NOT NULL,
  `archivo` varchar(1500) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `respuesta` varchar(1500) NOT NULL,
  `detalle_respuesta` varchar(1500) NOT NULL,
  `acuse_idacuse` int(11) DEFAULT 1,
  `enlace` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cerrado` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_solicitud`),
  KEY `acuse_idacuse` (`acuse_idacuse`),
  CONSTRAINT `solicitudes_ibfk_1` FOREIGN KEY (`acuse_idacuse`) REFERENCES `acuse` (`id_acuse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudes`
--

LOCK TABLES `solicitudes` WRITE;
/*!40000 ALTER TABLE `solicitudes` DISABLE KEYS */;
INSERT INTO `solicitudes` VALUES (3,'Reunión con Lopez-Gatell','Se debe tratar temas diversos respecto al regreso seguro y la pandemia COVID-19','2022-01-18','/public/uploads/solicitudes/3/fw8ben.pdf',2,'No viable','El Dr. tiene la agenda llena para esa fecha',3,0,1,1,1),(4,'Visita a la UNAM','Visita para desarrollar vacuna antiCOVID','2022-01-27','/public/uploads/solicitudes/4/W9_INSTRUCTION.pdf',0,'','',1,0,1,1,1),(6,'Fiesta de San Valentin 2022','Se propone hacer una fiesta para regresar la alegría después de tanto encierro','2022-02-14','/public/uploads/solicitudes/6/dummy.pdf',1,'Favorable','Necesitamos más datos, pero sí procede',4,0,2,1,1);
/*!40000 ALTER TABLE `solicitudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_consejero`
--

DROP TABLE IF EXISTS `tipo_consejero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_consejero` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_consejero`
--

LOCK TABLES `tipo_consejero` WRITE;
/*!40000 ALTER TABLE `tipo_consejero` DISABLE KEYS */;
INSERT INTO `tipo_consejero` VALUES (1,'Propietario',1),(2,'Suplente',1);
/*!40000 ALTER TABLE `tipo_consejero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_admin`
--

DROP TABLE IF EXISTS `usuarios_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_admin` (
  `idusuarios_admin` int(11) NOT NULL AUTO_INCREMENT,
  `rol` int(11) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `passwd` varchar(500) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `daua` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusuarios_admin`),
  KEY `rol` (`rol`),
  KEY `daua` (`daua`),
  CONSTRAINT `usuarios_admin_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `roles` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarios_admin_ibfk_2` FOREIGN KEY (`daua`) REFERENCES `daua` (`id_daua`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_admin`
--

LOCK TABLES `usuarios_admin` WRITE;
/*!40000 ALTER TABLE `usuarios_admin` DISABLE KEYS */;
INSERT INTO `usuarios_admin` VALUES (1,1,'admin','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,NULL),(2,2,'enlace','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,NULL),(3,3,'secretario','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,NULL),(4,4,'sadmin','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,1);
/*!40000 ALTER TABLE `usuarios_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_consejeros`
--

DROP TABLE IF EXISTS `usuarios_consejeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_consejeros` (
  `id_usuarios_consejeros` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(20) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `passwd` varchar(500) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `passwd_cambio` int(11) NOT NULL DEFAULT 0,
  `rol_idrol` int(11) NOT NULL DEFAULT 5,
  `info_cambio` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_usuarios_consejeros`),
  KEY `id` (`id`),
  KEY `rol_idrol` (`rol_idrol`),
  CONSTRAINT `usuarios_consejeros_ibfk_1` FOREIGN KEY (`rol_idrol`) REFERENCES `roles` (`id_rol`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_consejeros`
--

LOCK TABLES `usuarios_consejeros` WRITE;
/*!40000 ALTER TABLE `usuarios_consejeros` DISABLE KEYS */;
INSERT INTO `usuarios_consejeros` VALUES (1,'200914922','alvaro','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,0,5,0),(2,'201040900','pedro','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,0,5,0),(3,'201214833','giovana','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,0,5,0),(4,'201535852','mariana','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,0,5,0),(5,'200626887','felipe','$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2',1,0,5,0);
/*!40000 ALTER TABLE `usuarios_consejeros` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-10 13:08:55
