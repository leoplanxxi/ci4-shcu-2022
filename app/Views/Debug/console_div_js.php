<div id="logger" style="border: 1px solid black; padding: 10px;">
<b>Consola JS</b><br>
</div>
<script type="text/javascript">
(function () {
    var old = console.log;
    var logger = document.getElementById('logger');
    console.log = function (message) {
        if (typeof message == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br />';
        } else {
            logger.innerHTML += message + '<br />';
        }
    }
})();

    </script>
