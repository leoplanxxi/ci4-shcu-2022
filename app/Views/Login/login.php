<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3" style="background: rgb(210,210,210)">
			<p>&nbsp;</p>
			<h3 class="text-center">Inicio de Sesión</h1>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>	
			<?php echo form_open(); ?>
			<div class="form-group">
				<label for="matricula">Matrícula</label>
				<input type="text" class="form-control" name="matricula">
			</div>
			<div class="form-group">
				<label for="passwd">Contraseña</label>
				<input type="password" class="form-control" name="pass">
			</div>
			<p class="text-center"><input type="submit" value="Ingresar" class="btn btn-primary" class="text-center"></p>
			<?php echo form_close(); ?>
			
		</div>
	</div>
</div>
