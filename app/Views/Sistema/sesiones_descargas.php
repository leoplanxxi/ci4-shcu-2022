<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblSesionesArchivos").DataTable({ responsive: true });
	});
</script>
<div class="container">
<div class="row">
<div class="col-lg-12">
<p>&nbsp;</p>
<h2 class="text-center">Sesiones (Archivos)</h2>
<?php /// FALTA AGREGAR NOMBRE DE LA SESION ?>
<p>&nbsp;</p>
<?php if (session()->getFlashdata("msg")): ?>
<div class="alert alert-secondary">
	<?php echo session()->getFlashdata("msg"); ?>
</div>
<?php endif; ?>
<p>&nbsp;</p>
<a href="javascript:history.back(-1);" class="btn btn-primary">Regresar</a>
<p>&nbsp;</p>
<table id="tblSesionesArchivos">
<thead>
	<tr><th>Nombre</th><th>Fecha descarga</th></tr>
</thead>
<tbody>

	<?php foreach ($descargas as $d): ?>
		<tr><td><?php echo htmlspecialchars_decode($d->usuario); ?> (<?php echo $d->id; ?>)</td><td><?php echo $d->fecha_descarga; ?></td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>

</div>
</div>
</div>
