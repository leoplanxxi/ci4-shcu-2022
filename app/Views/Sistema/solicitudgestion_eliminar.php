<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<p>&nbsp;</p>
			<div class="card">
				<div class="card-header"><h2 class="text-center">Eliminar</h2></div>
				<div class="card-body">
					<p>¿Confirma que desea eliminar la solicitud con folio <?php echo $id; ?>?. Esta acción no puede revertirse.</p>
					<?php echo form_open(); ?>
					<p class="text-center">
						<input type="hidden" name="id_solicitud" value="<?php echo $id; ?>">
						<input type="submit" class="btn btn-danger" value="Aceptar"> &nbsp;
						<a href="javascript:history.back(1)" class="btn btn-outline-primary">Cancelar</a>
					</p>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
