<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblDirectorio").DataTable({ responsive: true });
	});
</script>
<div class="container">
<div class="row">
<div class="col-lg-12">
<p>&nbsp;</p>
<h2 class="text-center">Directorio</h2>
<p>&nbsp;</p>
<?php if (session()->getFlashdata("msg")): ?>
<div class="alert alert-secondary">
	<?php echo session()->getFlashdata("msg"); ?>
</div>
<?php endif; ?>
<p>&nbsp;</p>
<table id="tblDirectorio">
<thead><?php // Agregar foto ?>
	<tr><th>Matrícula</th><th>Nombre</th><th>Unidad Académica</th><th>Cargo</th><th>Tipo</th><th>Comisión</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($consejeros as $c): ?>
		<tr>
			<td><?php echo $c->id; ?></td>
			<td><?php echo htmlspecialchars_decode($c->nombre); ?></td>
			<td><?php echo $c->daua; ?></td>
			<td><?php echo $c->cargo; ?></td>
			<td><?php echo $c->tipo; ?></td>
			<td><?php echo $c->comision; ?></td>
			<td><a href="<?php echo base_url('directorio/expediente/'.$c->id); ?>" class="btn btn-primary"><i class="fas fa-file-archive"></i></a>
		</td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>

</div>
</div>
</div>
