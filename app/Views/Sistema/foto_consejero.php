<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<p>&nbsp;</p>
			<h2 class="text-center">Foto</h2>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<?php if ($foto_existente != "" && $foto_existente!== NULL):?>
			<p style="text-center"><img src="<?php echo $foto_existente; ?>"></p>
			<?php endif; ?>
			<?php echo form_open_multipart(); ?>

			<div class="form-group">
			<label for="foto">Fotografía</label>
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<input type="file" name="foto" class="form-control">
			<small>La fotografía deberá tener resolución 150x200, y en caso de ser más grade será redimensionada a esta resolución.</small>
			</div>
			<p class="text-center"><input type="submit" value="Guardar" class="btn btn-primary"></p>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
