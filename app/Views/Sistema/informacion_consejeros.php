<style>
.container-info .col-lg-6 .row { padding-top: 10px; padding-bottom: 10px; }
form {width:100%;}
</style>
<?php // FALTA CHECKBOX Y SELECT 

 ?>

<div class="container container-info">
	<div class="row">
		<?php echo form_open(); ?>

		<div class="col-lg-12">
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
				<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
				</div>
			<?php endif; ?>
			<div class="row">
			<div class="col-lg-6">
			<p><b>Datos personales:</b></p>
				<div class="row">
					<div class="col-lg-4">
					Matrícula:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->id; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Grado:
					</div>
					<div class="col-lg-8">
					<select name="grado" class="form-control">
						<option value="C." <?php if ($informacion->grado == "C.") echo "selected"; ?>>C.</option>
						<option value="Lic." <?php if ($informacion->grado == "Lic.") echo "selected"; ?> >Lic.</option>
						<option value="M. en C." <?php if ($informacion->grado == "M. en C.") echo "selected"; ?>>M. en C.</option>
						<option value="Dr." <?php if ($informacion->grado == "Dr.") echo "selected"; ?>>Dr.</option>
					</select>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Nombre (s):
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->nombre; ?>
					</div>
				</div>	
				<div class="row">
					<div class="col-lg-4">
					Apellido paterno:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->apellido_paterno; ?>
					</div>
				</div>				
				<div class="row">
					<div class="col-lg-4">
					Apellido materno:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->apellido_materno; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Cumpleaños*:
					</div>
					<div class="col-lg-8">
					<input type="date" name="cumpleanos" class="form-control" value="<?php echo set_value('cumpleanos',$informacion->cumpleanos); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Cargo:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->cargo; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Tipo:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->tipo; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Dependencia:
					</div>
					<div class="col-lg-8">
					<?php echo $informacion->daua_n; ?>
					</div>
				</div>
				<small>Si estos datos son incorrectos, favor de contactar al administrador.</small>
				<p>&nbsp;</p>
				<p><b>Teléfono</b></p>
				<div class="row">
					<div class="col-lg-4">
					Celular 1*:
					</div>
					<div class="col-lg-8">
					<input type="text" name="tel_cel_1" class="form-control" value="<?php echo set_value('tel_cel_1',$informacion->tel_cel_1); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					¿Este número cuenta con WhatsApp?:
					</div>
					<div class="col-lg-8">
					<input type="checkbox" name="tel_cel_1_whats" value="1" <?php if ($informacion->tel_cel_1_whats == "1") echo "checked"; ?>>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Celular 2:
					</div>
					<div class="col-lg-8">
					<input type="text" name="tel_cel_2" class="form-control" value="<?php echo set_value('tel_cel_2',$informacion->tel_cel_2); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					¿Este número cuenta con WhatsApp?:
					</div>
					<div class="col-lg-8">
					<input type="checkbox" name="tel_cel_2_whats" value="1" <?php if ($informacion->tel_cel_2_whats == "1") echo "checked"; ?>>
					</div>
				</div>		
				<div class="row">
					<div class="col-lg-4">
					Casa*:
					</div>
					<div class="col-lg-8">
					<input type="text" name="tel_casa" class="form-control" value="<?php echo set_value('tel_casa', $informacion->tel_casa); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Oficina:
					</div>
					<div class="col-lg-8">
					<input type="text" name="tel_oficina" class="form-control" value="<?php echo set_value('tel_oficina', $informacion->tel_oficina); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Extensión:
					</div>
					<div class="col-lg-8">
					<input type="text" name="extension" class="form-control" value="<?php echo set_value('extension', $informacion->extension); ?>">
					</div>
				</div>							
			</div>
			<div class="col-lg-6">
				<p><b>Correo electrónico</b></p>
				<div class="row">
					<div class="col-lg-4">
					Correo Institucional:
					</div>
					<div class="col-lg-8">
					<input type="text" name="correo_institucional" class="form-control" value="<?php echo set_value('correo_institucional', $informacion->correo_institucional); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Correo Personal*:
					</div>
					<div class="col-lg-8">
					<input type="text" name="correo_personal" class="form-control" value="<?php echo set_value('correo_personal', $informacion->correo_personal); ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Comisión Estatutaria:
					</div>
					<div class="col-lg-8">
					<select name="comision" class="form-control">
						<?php foreach ($comisiones as $c): ?>
							<option value="<?php echo $c->id_comisiones; ?>" <?php 
								if ($informacion->comision_id == $c->id_comisiones) {
									echo "selected";
								}  ?>><?php echo $c->nombre; ?></option>
						<?php endforeach; ?>
					</select>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
					Toma de protesta*:
					</div>
					<div class="col-lg-8">
					<input type="date" name="protesta" class="form-control" value="<?php echo set_value('protesta', $informacion->protesta); ?>">
					</div>
				</div><br><br>
				<small>* Campos obligatorios</small>
			</div></div>
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<input type="submit" value="Guardar" class="btn btn-primary" style="float:right;">
			<?php echo form_close(); ?>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
	</div>
</div>
