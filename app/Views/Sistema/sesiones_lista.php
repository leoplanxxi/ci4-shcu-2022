<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblSesiones").DataTable({ responsive: true });
	});
</script>
<div class="container">
<div class="row">
<div class="col-lg-12">
<p>&nbsp;</p>
<h2 class="text-center">Sesiones</h2>
<p>&nbsp;</p>
<?php if (session()->getFlashdata("msg")): ?>
<div class="alert alert-secondary">
	<?php echo session()->getFlashdata("msg"); ?>
</div>
<?php endif; ?>
<?php if ($rol == 2 || $rol == 3): ?>
<a href="<?php echo base_url('sesiones/crear'); ?>" class="btn btn-success"><i class="fas fa-plus"></i> Agregar sesión</a> <a href="<?php echo base_url('sesiones/aviso'); ?>" class="btn btn-primary"><i class="fas fa-exclamation-triangle"></i> Aviso de sesión</a>
<p>&nbsp;</p>
<table id="tblSesiones">
<thead>
	<tr><th>Nombre</th><th>Fecha</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($sesiones as $s): ?>
		<tr><td><?php echo htmlspecialchars_decode($s->nombre); ?></td><td><?php echo $s->fecha; ?></td>
			<td><?php /*<a href="#" class="btn btn-primary"><i class="fas fa-pencil"></i></a> */ ?>
			<a href="<?php echo base_url('Sesiones/asistentes/'.$s->id_sesion);?>" class="btn btn-primary"><i class="fas fa-users"></i></a>
			<a href="<?php echo base_url('Sesiones/archivos/'.$s->id_sesion); ?>" class="btn btn-primary"><i class="fas fa-file"></i></a>
			<a href="<?php echo base_url('Sesiones/eliminar_sesion/'.$s->id_sesion); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php elseif ($rol == 4): ?>
<p>&nbsp;</p>
<table id="tblSesiones">
<thead>
	<tr><th>Nombre</th><th>Fecha</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($sesiones as $s): ?>
		<tr><td><?php echo htmlspecialchars_decode($s->nombre); ?></td><td><?php echo $s->fecha; ?></td>
			<td><?php /*<a href="#" class="btn btn-primary"><i class="fas fa-pencil"></i></a> */ ?>
			<a href="<?php echo base_url('Sesiones/reporte_asistentes/'.$s->id_sesion);?>" class="btn btn-primary"><i class="fas fa-users"></i></a>
			<a href="<?php echo base_url('Sesiones/archivos/'.$s->id_sesion); ?>" class="btn btn-primary"><i class="fas fa-file"></i></a>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php elseif ($rol == 5): ?>
<p>&nbsp;</p>
<table id="tblSesiones">
<thead>
	<tr><th>Nombre</th><th>Fecha</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($sesiones as $s): ?>
		<tr><td><?php echo htmlspecialchars_decode($s->nombre); ?></td><td><?php echo $s->fecha; ?></td>
			<td><a href="<?php echo base_url('Sesiones/archivos/'.$s->id_sesion); ?>" class="btn btn-primary"><i class="fas fa-file"></i></a></td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php endif; ?>

</div>
</div>
</div>
