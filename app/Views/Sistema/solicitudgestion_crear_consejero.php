<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p>&nbsp;</p>
			<h2 class="text-center">Solicitudes de gestión</h2>
			<h3 class="text-center">Crear solicitud</h3>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<?php echo form_open_multipart(); ?>
				<div class="form-group">
					<label for="nombre">Nombre del evento:</label>
					<input type="text" name="nombre" class="form-control">
				</div>
				<div class="form-group">
					<label for="descripcion">Descripción:</label>
					<textarea name="descripcion" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label for="fecha">Fecha del evento:</label>
					<input type="date" name="fecha" class="form-control">
				</div>
				<div class="form-group">
					<label for="archivo">Documentos escaneados:</label>
					<input type="file" name="archivo" class="form-control">
					<small class="form-text text-muted">Para subir múltiples archivos, subirlos en un archivo en formato comprimido (zip).</small>
				</div>
				<div class="text-center"><input type="submit" class="btn btn-primary" value="Agregar"> <a href="javascript:history.back(1)" class="btn btn-outline-primary">Regresar</a></div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
