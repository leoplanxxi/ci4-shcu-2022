<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<p>&nbsp;</p>
			<h2 class="text-center">Agregar archivo</h2>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>		
			<p>&nbsp;</p>
			<?php echo form_open_multipart(); ?>
			<div class="form-group"><label for="nombre">Nombre:</label>
			<input type="text" name="nombre" class="form-control"></div>
			<div class="form-group"><label for="descripcion">Descripción</label>
			<input type="text" name="descripcion" class="form-control"></div>
			<div class="form-group"><label for="archivo">Archivo:</label>
			<input type="file" name="archivo" class="form-control"></div>
			<input type="hidden" name="id_sesion" value="<?php echo $id_sesion; ?>">
			<div class="text-center"><input type="submit" class="btn btn-primary" value="Agregar"> <a href="<?php echo base_url('sesiones/archivos/'.$id_sesion); ?>" class="btn btn-outline-primary">Regresar</a></div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
