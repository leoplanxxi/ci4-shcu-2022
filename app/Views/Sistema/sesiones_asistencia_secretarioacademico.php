<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	var ids = [];
	var ids_h = [];
	function textID() {
		var radios = document.getElementsByTagName('input');
		ids = [];
		ids_h=[];
		for (i=0;i<radios.length;i++) {
			if (radios[i].type=="radio" && radios[i].checked) {
				if (!radios[i].value.startsWith('titular')) {
					matr = radios[i].value.split('_');

					matr.forEach(function(element) {
						if (element != 'suplente' && element!='ninguno') {
							if (!ids.includes(element)) {
								ids.push(element);	
							}
						}	
					});

				}
			}
		}

		justifs = document.getElementById('justif');
		justifs.innerHTML = "";
		ids.forEach(function(element) {
			if (!ids_h.includes(element)) {
				justifs.innerHTML += "<div class='card'><div class='card-body'><p>Justificante para usuario con ID: " + element + "</p><div class='form-group'></div><input type='hidden' name='id_" + element + "' value='"+element+"'><input type='file' name='justificante_" + element + "' class='form-control'><small>Para subir múltiples archivos, subirlos en un archivo en formato comprimido (zip).</small></div></div>";
				ids_h.push(element);

			}
		});
	/*	ids_h.push(ids);*/

		console.log(ids_h);
	}
	
	$(document).ready(function() {
		$("#tblAsistencia").DataTable({ responsive: true, ordering: false, });
	});
	
</script>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p>&nbsp;</p>
			<h2 class="text-center">Asistencias</h2>
			<h3 class="text-center">Sesión</h3>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<pre>
			<?php 
			$arr_pr = array();
			$visitados = [];
			foreach ($asistentes as $key_c=>$val_c) {
				if (!in_array($key_c, $visitados)) {
					//array_push($key_c, $visitados);
					if ($val_c->tipo == "Propietario") {
						$suplente = $val_c->asociado;
						array_push($visitados, $key_c);
						array_push($visitados, $suplente);
					}
					array_push($arr_pr,["P" => $key_c, "S" => $suplente]);
				}
			}
			
			?>
			</pre>
			<p>Seleccione los consejeros que asistirán a la presente sesión:</p>
			<?php echo form_open_multipart(); ?>
			<input type="hidden" name="id_sesion" value="<?php echo $id_sesion; ?>">
			<input type="submit" class="btn btn-primary col-lg-6 offset-lg-3" value="Guardar">
			<p>&nbsp;</p>
			<table id="tblAsistencia">
				<thead>
					<tr><th>&nbsp;</th><th>Titular</th><th>Suplente</th><th>Ninguno</th></tr>
				</thead>
				<tbody>
					<?php $i=0; ?>
					<?php foreach ($arr_pr as $p): ?>
					<tr><td><?php echo $asistentes->{$p["P"]}->cargo; ?><br><b>Titular:</b> <span id="nombre-<?php echo $p['P']; ?>"><?php echo $asistentes->{$p["P"]}->nombre; ?> (<?php echo $p['P']; ?>)</span><br><b>Suplente:</b> <span id="nombre-<?php echo $p['S']; ?>"><?php echo $asistentes->{$p["S"]}->nombre; ?> (<?php echo $p['S']; ?>)</span></td>
					<td><input type="radio" name="asistencia-<?php echo $i; ?>" value="titular_<?php echo $p['P']; ?>" <?php if ($asistentes->{$p["P"]}->resp == 1) { echo "checked"; } ?> onchange="javascript:textID()"></td>
					<td><input type="radio" name="asistencia-<?php echo $i; ?>" value="suplente_<?php echo $p['S']; ?>" <?php if ($asistentes->{$p["S"]}->resp == 1) { echo "checked"; } ?> onchange="javascript:textID()"></td>
					<td><input type="radio" name="asistencia-<?php echo $i; ?>" value="ninguno_<?php echo $p['P']; ?>_<?php echo $p['S']; ?>"  <?php if ($asistentes->{$p["P"]}->resp == 0 && $asistentes->{$p["S"]}->resp == 0) { echo "checked"; } ?> onchange="javascript:textID()"></td></tr><?php $i++; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
			<p>&nbsp;</p>
			<div id="justif">
			</div>
			
		
			</div><p>&nbsp;</p>
			<input type="submit" class="btn btn-primary col-lg-6 offset-lg-3" value="Guardar">

			<?php echo form_close(); ?>
		</div>
	</div>
</div>
