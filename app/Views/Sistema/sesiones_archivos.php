<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblSesionesArchivos").DataTable({ responsive: true });
	});
</script>
<div class="container">
<div class="row">
<div class="col-lg-12">
<p>&nbsp;</p>
<h2 class="text-center">Sesiones (Archivos)</h2>
<?php /// FALTA AGREGAR NOMBRE DE LA SESION ?>
<p>&nbsp;</p>
<?php if (session()->getFlashdata("msg")): ?>
<div class="alert alert-secondary">
	<?php echo session()->getFlashdata("msg"); ?>
</div>
<?php endif; ?>
<?php if ($rol == 2 || $rol == 3): ?>
<a href="<?php echo base_url('sesiones/lista'); ?>" class="btn btn-primary">Regresar a Sesiones</a> <a href="<?php echo base_url('sesiones/archivos_subir/'.$id_sesion); ?>" class="btn btn-success"><i class="fas fa-plus"></i> Agregar archivo</a>
<p>&nbsp;</p>
<table id="tblSesionesArchivos">
<thead>
	<tr><th>Nombre</th><th>Descripcion</th><th>Fecha</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php if ($archivos !== []): ?>
	<?php foreach ($archivos as $a): ?>
		<tr><td><?php echo htmlspecialchars_decode($a->nombre); ?></td><td><?php echo $a->descripcion; ?></td><td><?php echo $a->fecha_subida; ?></td>
			<td><a href="<?php echo base_url($a->archivo); ?>" class="btn btn-primary" target="_blank"><i class="fas fa-download"></i></a>
			<a href="<?php echo base_url('sesiones/bitacora_descargas/'.$a->id_archivo_material_sesion); ?>" class="btn btn-primary"><i class="fas fa-cloud-download"></i></a>
			<a href="<?php echo base_url('sesiones/eliminar_archivo/'.$a->id_archivo_material_sesion.'/'.$id_sesion); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
</tbody>
</table>
<?php elseif ($rol == 5): ?>
<a href="<?php echo base_url('sesiones/lista');?>" class="btn btn-primary">Regresar a sesiones</a>
<p>&nbsp;</p>
<table id="tblSesionesArchivos">
<thead>
	<tr><th>Nombre</th><th>Descripcion</th><th>Fecha</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php if ($archivos !== []): ?>
	<?php foreach ($archivos as $a): ?>
		<tr><td><?php echo htmlspecialchars_decode($a->nombre); ?></td><td><?php echo $a->descripcion; ?></td><td><?php echo $a->fecha_subida; ?></td>
		<?php $usr_id = $this->session->get("user_info")["idtbl"]; ?>
			<td><a href="<?php echo base_url('sesiones/descargar/'.$usr_id.'/'.$a->id_archivo_material_sesion); ?>" target="_blank" class="btn btn-primary"><i class="fas fa-download"></i></a>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
</tbody>
</table>
<?php endif; ?>
</div>
</div>
</div>
