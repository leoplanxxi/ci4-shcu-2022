<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblAsistencia").DataTable({ responsive: true });
	});
</script>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p>&nbsp;</p>
			<h2 class="text-center">Asistencia</h2>
			<h3 class="text-center">Sesion</h3>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<p>&nbsp;</p>
			<a href="<?php echo base_url('sesiones/lista');?>" class="btn btn-primary">Regresar a sesiones</a>
			<?php echo form_open(); ?>
			<input type="hidden" name="id_sesion" value="<?php echo $id_sesion; ?>">
			<input type="submit" class="btn btn-primary col-lg-6 offset-lg-3" value="Guardar">
			<p>&nbsp;</p>
			<table id="tblAsistencia">
				<thead>
					<tr><th>Matrícula</th><th>Nombre</th><th>UA</th><th>Cargo</th><th>Asistencia</th><th>A favor</th><th>En contra</th><th>Abstención</th><th>Operaciones</th></tr>
				</thead>
				<tbody>
				<?php foreach ($asistencias as $key => $value): ?>
					<?php foreach ($value as $a): ?>
						<?php if ($a->resp == 1): ?>
						<tr>
							<td><?php echo $a->id; ?></td>
							<td><?php echo $a->nombre; ?></td>
							<td><?php echo $a->daua; ?></td>
							<td><?php echo $a->cargo; ?><br>(<?php echo $a->tipo; ?>)</td>
							<td><input type="hidden" name="asistencia_<?php echo $a->id_daua; ?>_<?php echo $a->id; ?>" value="0"><input type="checkbox" name="asistencia_<?php echo $a->id_daua; ?>_<?php echo $a->id; ?>" value="1" <?php if ($a->asistencia == 1) { echo "checked"; } ?>></td>
							<td><input type="radio" name="voto_<?php echo $a->id_daua; ?>_<?php echo $a->id; ?>" value="1" <?php if ($a->voto == 1) { echo "checked"; }?>></td>
							<td><input type="radio" name="voto_<?php echo $a->id_daua; ?>_<?php echo $a->id; ?>" value="2"  <?php if ($a->voto == 2) { echo "checked"; }?>></td>
							<td><input type="radio" name="voto_<?php echo $a->id_daua; ?>_<?php echo $a->id; ?>" value="3"  <?php if ($a->voto == 3) { echo "checked"; }?>></td>
							<td></td>
						</tr>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
