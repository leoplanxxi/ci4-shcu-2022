<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p>&nbsp;</p>
			<h2 class="text-center">Sesión</h2>
			<h3 class="text-center">Crear sesión</h3>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<?php echo form_open_multipart(); ?>
				<div class="form-group">
					<label for="nombre">Nombre de la sesión:</label>
					<input type="text" name="nombre" class="form-control">
				</div>
				<div class="form-group">
					<label for="fecha">Fecha de la sesión:</label>
					<input type="date" name="fecha" class="form-control">
				</div>
				
				<div class="text-center"><input type="submit" class="btn btn-primary" value="Crear"> <a href="javascript:history.back(1)" class="btn btn-outline-primary">Regresar</a></div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
