<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<p>&nbsp;</p>
			<h2 class="text-center">Aviso de sesión</h2>
			<p>&nbsp;</p>
			<?php 
			function br2nl( $input ) {
			    return preg_replace('/<br\s?\/?>/ius', "\n", str_replace("\n","",str_replace("\r","", htmlspecialchars_decode($input))));
			}
			?>
			<?php echo form_open(); ?>
			<textarea class="form-control" rows=5 name="aviso"><?php echo br2nl($aviso); ?></textarea>
			<p>&nbsp;</p>
			<div class="form-group">
				<label for="sesion_activa">Sesión activa:</label>
				<select name="sesion_activa" class="form-control">
					<option> </option>
					<?php foreach ($sesiones as $s): ?>
					<option value="<?php echo $s->id_sesion; ?>" <?php if ($sesion_activa->valor == $s->id_sesion) echo "selected='selected'"; ?>><?php echo $s->nombre . " "; ?>(<?php echo $s->fecha; ?>)</option>
					<?php endforeach; ?>
				</select>
			</div>
			<p class="text-center"><input type="submit" value="Guardar" class="btn btn-success"> <a href="<?php echo base_url('sesiones/lista'); ?>" class="btn btn-outline-primary">Regresar</a></p>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
