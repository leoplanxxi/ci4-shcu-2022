<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<p>&nbsp;</p>
			<h2 class="text-center">Solicitudes de gestión</h2>
			<h3 class="text-center">Detalles</h3>
			<p>&nbsp;</p>
			<?php if (session()->getFlashdata("msg")): ?>
			<div class="alert alert-secondary">
				<?php echo session()->getFlashdata("msg"); ?>
			</div>
			<?php endif; ?>
			<p><b>Nombre del solicitante: </b> <?php echo $informacion->id_usuario; ?></p>
			<p><b>Dependencia: </b> <?php echo $informacion->daua; ?></p>
			<hr>
			<p><b>Folio de seguimiento: </b> <?php echo $informacion->id_solicitud; ?></p>
			<p><b>Nombre del evento: </b> <?php echo htmlspecialchars_decode($informacion->nombre); ?></p>
			<p><b>Descripción del evento: </b><br> <?php echo htmlspecialchars_decode($informacion->descripcion); ?></p>
			<p><b>Fecha del evento: </b> <?php echo $informacion->fecha; ?></p>
			<p><b>Documento enviado: </b> <a href="<?php echo base_url($informacion->archivo); ?>"><?php echo str_replace("/public/uploads/solicitudes/".$informacion->id_solicitud."/","",$informacion->archivo); ?></a></p>
			<hr>
			<?php if (($rol == 3 || $rol == 2) && ($informacion->cerrado != 1)): ?>
			<?php echo form_open(); ?>
			<input type="hidden" name="id_solicitud" value="<?php echo $informacion->id_solicitud; ?>">
			<div class="form-group"><label for="acuse_idacuse">Acuse de recibido:</label><br>
			<select name="acuse_idacuse" class="form-control">
				<?php foreach ($acuses as $a) {
					if ($informacion->id_acuse == $a->id_acuse) echo "<option value='$a->id_acuse' selected='selected'>$a->nombre</option>";
					else echo "<option value='$a->id_acuse'>$a->nombre</option>";
				}?>
			</select></div>
			<div class="form-group"><label for="estado">Estado:</label><br>
			<select name="estado" class="form-control">
				<option value="0" <?php if ($informacion->raw_estado == 0) echo "selected='selected'"; ?>>Recibido</option>
				<option value="1" <?php if ($informacion->raw_estado == 1) echo "selected='selected'"; ?>>En seguimiento</option>
				<option value="2" <?php if ($informacion->raw_estado == 2) echo "selected='selected'"; ?>>Respuesta</option>
			</select></div>	
			<div class="form-group"><label for="respuesta">Respuesta:</label><br>
			<select name="respuesta" class="form-control">
				<option value="Sin respuesta" <?php if ($informacion->respuesta == "Sin respuesta") echo "selected='selected'"; ?>>Sin respuesta</option>
				<option value="Favorable" <?php if ($informacion->respuesta == "Favorable") echo "selected='selected'"; ?>>Favorable</option>
				<option value="No viable" <?php if ($informacion->respuesta == "No viable") echo "selected='selected'"; ?>>No viable</option>
				<option value="No favorable" <?php if ($informacion->respuesta == "No favorable") echo "selected='selected'"; ?>>No favorable</option>
			</select></div>		
			<div class="form-group"><label for="detalle_respuesta">Detalle de respuesta:</label><br>
			<textarea name="detalle_respuesta" class="form-control"><?php echo $informacion->detalle_respuesta; ?></textarea></div>		
			<p><a href="<?php echo base_url('solicitudes/lista'); ?>" class="btn btn-outline-primary float-left">Regresar a la lista de solicitudes</a>  <input type="submit" class="btn btn-success float-right" value="Enviar respuesta"><?php if ($rol==3): ?>&nbsp;<a href="<?php echo base_url('solicitudes/cerrar/'.$informacion->id_solicitud); ?>" class="btn-danger btn float-right">Cerrar solicitud</a><?php endif; ?></p>
			<?php echo form_close(); ?>
			<?php elseif (($rol == 3 || $rol == 2) && ($informacion->cerrado == 1)): ?>
			<div class="alert alert-danger">La solicitud ha sido cerrada por el secretario técnico</div>
			<p><b>Acuse de recibido: </b><?php echo $informacion->acuse_idacuse; ?></p>
			<p><b>Estado:</b> <?php echo $informacion->estado; ?></p>
			<p><b>Respuesta: </b><?php echo $informacion->respuesta; ?></p>
			<p><b>Detalle de respuesta:</b> <?php echo $informacion->detalle_respuesta; ?></p>
			<p><a href="<?php echo base_url('solicitudes/lista'); ?>" class="btn btn-outline-primary">Regresar a lista de solicitudes</a></p>
			<?php elseif ($rol == 5): ?>
			<?php if ($informacion->cerrado == 1) {
				echo '<div class="alert alert-danger">La solicitud ha sido cerrada por el secretario técnico</div>';
			} ?>
			<p><b>Acuse de recibido: </b><?php echo $informacion->acuse_idacuse; ?></p>
			<p><b>Estado:</b> <?php echo $informacion->estado; ?></p>
			<p><b>Respuesta: </b><?php echo $informacion->respuesta; ?></p>
			<p><b>Detalle de respuesta:</b> <?php echo $informacion->detalle_respuesta; ?></p>
			<p><a href="<?php echo base_url('solicitudes/lista'); ?>" class="btn btn-outline-primary">Regresar a lista de solicitudes</a></p>
			<?php endif; ?>
		</div>
	</div>
</div>
