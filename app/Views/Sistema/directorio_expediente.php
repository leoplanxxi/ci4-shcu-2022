<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p>&nbsp;</p>
			<h2 class="text-center">Expediente de Consejero</h2>
			<p>&nbsp;</p>
			<a href="<?php echo base_url('directorio/lista'); ?>" class="btn btn-primary">Regresar a directorio</a>
			<p>&nbsp;</p>

			<hr>
			<div class="col-lg-8 offset-lg-2"><div class="row">
				<div class="col-lg-6"><b>Nombre:</b></div><div class="col-lg-6"><?php echo $expediente->nombre . " " . $expediente->apellido_paterno . " " . $expediente->apellido_materno; ?></div><br><br>
				<div class="col-lg-6"><b>Matrícula / No. de Trabajador:</b></div><div class="col-lg-6"><?php echo $expediente->id; ?></div><br><br>
				<div class="col-lg-6"><b>Cumpleaños:</b></div><div class="col-lg-6"><?php echo $expediente->cumpleanos; ?></div><br><br>
				<div class="col-lg-6"><b>Cargo:</b></div><div class="col-lg-6"><?php echo $expediente->cargo; ?></div><br><br>
				<div class="col-lg-6"><b>Tipo:</b></div><div class="col-lg-6"><?php echo $expediente->tipo; ?></div><br><br>
				<div class="col-lg-6"><b>Celular:</b></div><div class="col-lg-6"><?php echo $expediente->tel_cel_1; ?></div><br><br>
				<div class="col-lg-6"><b>Correo Personal:</b></div><div class="col-lg-6"><?php echo $expediente->correo_personal; ?></div><br><br>
				<div class="col-lg-6"><b>Correo Institucional:</b></div><div class="col-lg-6"><?php echo $expediente->correo_institucional; ?></div><br><br>
				<p>&nbsp;</p>
			</div></div>
			<h2 class="text-center">Sesiones</h2>
			<div class="col-lg-8 offset-lg-2"><div class="row">
				<table class="table">
					<thead>
						<tr><th>Sesión</th><th>Asistencia</th><th>Voto</th></tr>
					</thead>
					<tbody>
						<?php foreach ($expediente->sesiones as $s): ?>
						<tr><td><?php echo $s->sesion; ?></td><td><?php echo $s->asistencia; ?></td><td><?php echo $s->voto; ?></td></tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div></div>
			<p>&nbsp;</p>
			<h2 class="text-center">Solicitudes</h2>
			<div class="col-lg-8 offset-lg-2"><div class="row">
				<table class="table">
					<thead>
						<tr><th>Folio</th><th>Nombre</th><th>Estado</th><th>Respuesta</th></tr>
					</thead>
					<tbody>
						<?php foreach ($expediente->solicitudes as $s): ?>
						<tr><td><?php echo $s->id_solicitud; ?></td><td><?php echo $s->nombre; ?></td><td><?php echo $s->estado; ?></td><td><?php echo $s->respuesta; ?></td></tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div></div>
		</div>
	</div>
</div>
