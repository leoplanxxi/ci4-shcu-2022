<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script>
	$(document).ready(function() {
		$("#tblSols").DataTable({ responsive: true });
	});
</script>
<div class="container">
<div class="row">
<div class="col-lg-12">
<p>&nbsp;</p>
<h2 class="text-center">Solicitudes de gestión</h2>
<p>&nbsp;</p>
<?php if (session()->getFlashdata("msg")): ?>
<div class="alert alert-secondary">
	<?php echo session()->getFlashdata("msg"); ?>
</div>
<?php endif; ?>
<?php if ($rol == 2 || $rol == 3): ?>
<table id="tblSols">
<thead>
	<tr><th>Folio</th><th>Nombre</th><th>Fecha</th><th>Solicitante</th><th>Dependencia</th><th>Estado</th><th>Respuesta</th><th>Activo</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($solicitudes as $s): ?>
		<tr><td><?php echo $s->id_solicitud; ?></td><td><?php echo htmlspecialchars_decode($s->nombre); ?></td><td><?php echo $s->fecha; ?></td><td><?php echo $s->id_usuario; ?></td><td><?php echo $s->daua; ?></td><td><?php echo $s->estado; ?></td><td><?php echo $s->respuesta; ?></td><td><?php if ($s->cerrado == 0) echo "Activo"; else echo "Cerrado"; ?></td>
			<?php if ($rol == 3 || $rol == 2): ?>
			<td><a href="<?php echo base_url('solicitudes/detalles/' . $s->id_solicitud); ?>" class="btn btn-success"><i class="fas fa-info"></a></td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php elseif ($rol == 5): ?>
<a href="<?php echo base_url('solicitudes/crear'); ?>" class="btn btn-success"><i class="fas fa-plus"></i> Agregar solicitud</a>
<p>&nbsp;</p>
<table id="tblSols">
<thead>
	<tr><th>Folio</th><th>Nombre</th><th>Fecha</th><th>Estado</th><th>Acuse</th><th>Respuesta</th><th>Operaciones</th></tr>
</thead>
<tbody>
	<?php foreach ($solicitudes as $s): ?>
		<tr><td><?php echo $s->id_solicitud; ?></td><td><?php echo htmlspecialchars_decode($s->nombre); ?></td><td><?php echo $s->fecha; ?></td><td><?php echo $s->estado; ?></td><td><?php echo $s->acuse_idacuse; ?></td><td><?php echo $s->respuesta; ?></td>
		<td>
			<?php /*<a href="#" class="btn btn-primary"><i class="fas fa-pencil"></i></a>  */?>
			<a href="<?php echo base_url('solicitudes/detalles/' . $s->id_solicitud); ?>" class="btn btn-primary"><i class="fas fa-info"></i></a> 
			<?php if ($s->cerrado != 1): ?>
			<a href="<?php echo base_url('solicitudes/eliminar/' . $s->id_solicitud); ?>" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
			<?php endif; ?>
		</td></tr>
	<?php endforeach; ?>
</tbody>
</table>
<?php endif; ?>
</div>
</div>
</div>
