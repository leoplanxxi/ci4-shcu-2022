<div class="container container-menu-hcu">
    <?php /*<a id="navbar-brand" class="navbar-brand" href="#">HCU</a> */ ?>

    <nav class="navbar navbar-expand-lg navbar-inverse navbar-hcu">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuHCU" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          
        </button>
    <div class="collapse navbar-collapse" id="menuHCU">
	    <ul class="navbar navbar-nav navbar-menu-hcu">
		<?php foreach($menu as $text=>$link): ?>
			<?php $link = explode("$$",$link);  ?>
			<li class="nav-item"><a class="nav-link" href="<?php echo $link[0]; ?>" <?php if (isset($link[1])) echo 'target=".$link[1]."'; ?>><?php echo $text; ?></a></li>
		<?php endforeach; ?>
	    </ul>
	</div>
	</nav>
</div>
<?php /*
 <script>
     var navbar_brand = document.getElementById("navbar-brand"); 
     var url = window.location.href.toString().split("/")[5];
     setBrand(url);
 
     window.onload = function(){
         if (window.innerWidth<992) {
             navbar_brand.style.display = "inline-block";
         }else{
             navbar_brand.style.display = "none";
         };
     };
     window.onresize = function(){
         if (window.innerWidth<992) {
             navbar_brand.style.display = "inline-block";
         }else{
             navbar_brand.style.display = "none";
         };
     };
     
     function setBrand(b){
         switch(b){
             case 'admin':
                 navbar_brand.innerHTML = "Inicio";
                 break;
             case 'sesiones':
                 navbar_brand.innerHTML = "Sesiones";
                 break;
             case 'boletines':
                 navbar_brand.innerHTML = "Boletines";
                 break;
             case 'solicitudes':
                 navbar_brand.innerHTML = "Solicitudes de Gestión";
                 break;
             case 'aviso':
                 navbar_brand.innerHTML = "Aviso de Sesión";
                 break;
             case 'usuarios':
                 navbar_brand.innerHTML = "Usuarios";
                 break;
             case 'dependencia':
                 navbar_brand.innerHTML = "Dependencias";
                 break;
             case 'directorio':
                 navbar_brand.innerHTML = "Directorio";
                 break;
             default:
                 //navbar_brand.innerHTML = "HCU";
         }
         
     };
 
 </script>
*/ ?>
