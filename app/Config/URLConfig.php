<?php
namespace Config;

use CodeIgniter\Config\BaseConfig;

class URLConfig extends BaseConfig {
	public $public_dir = "public/";

	public function getBase() {
		helper('url');
		return base_url();
	}
}
