<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Consejero extends BaseController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session(); 

		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		
		$this->libJWT = new libJWT();
		
		$jwt = $this->libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$this->session->set(['jwt_token' => $token]);
	
		$this->libHCU = new libHCU();

		$this->rol = $this->session->get("user_info")["rol"];
	}

	public function index() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$validation_rules = array('cumpleanos' => array('required','valid_date'), 'protesta' => array('required', 'valid_date'), 'tel_cel_1' => 'required', 'tel_casa' => 'required', 'correo_personal' => array('required', 'valid_email'));
		$menu_arr = $this->libHCU->menuRol($this->rol);
		
		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/expediente",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array(
					'id' => $this->session->get("user_info")["usr"],
				)
			);

			$resp=$this->libJWT->putData($arr_config);
			$resp=json_decode($resp);

			$data["informacion"] = json_decode($resp->data);
			$data["id"] = $this->session->get("user_info")["usr"];

			$arr_config = array(
				'URI' => "/API/get/comisiones",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array(),	
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			$data["comisiones"] = json_decode($resp->data);

			if (!$this->request->getPostGet()) {
				echo view("Sistema/informacion_consejeros", $data);
			}
			else {
				if (!$this->validate($validation_rules)) {
					$data = array('val_error'=>$this->validator->listErrors(),);
					$this->session->setFlashdata("msg",$this->validator->listErrors());
					return redirect()->to(base_url('consejero'));
				}
				else {
					$arr_config  = array(
						'URI' => "/API/upd/informacion_consejero",
						'auth' => $this->session->get("jwt_token"),
						'method' => 'GET',
						'args' => array(
       						'grado' => limpiarInputString($this->request->getPostGet("grado")),	
       						'cumpleanos' => $this->request->getPostGet("cumpleanos"),	
       						'tel_cel_1' => limpiarInputString($this->request->getPostGet("tel_cel_1")),	
       						'tel_cel_1_whats' => limpiarInputString($this->request->getPostGet("tel_cel_1_whats")),	       						
       						'tel_cel_2' => limpiarInputString($this->request->getPostGet("tel_cel_2")),	
       						'tel_cel_2_whats' => limpiarInputString($this->request->getPostGet("tel_cel_2_whats")),
       					    'tel_casa' => limpiarInputString($this->request->getPostGet("tel_casa")),	
       					    'tel_oficina' => limpiarInputString($this->request->getPostGet("tel_oficina")),	
       					    'extension' => limpiarInputString($this->request->getPostGet("extension")),	
       					    'correo_institucional' => limpiarInputString($this->request->getPostGet("correo_institucional")),	
       					    'correo_personal' => limpiarInputString($this->request->getPostGet("correo_personal")),	
       					    'comision' => limpiarInputString($this->request->getPostGet("comision")),	
       					    'protesta' => limpiarInputString($this->request->getPostGet("protesta")),	
       					    'id' => limpiarInputString($this->request->getPostGet("id")),
						),
					);

					$resp = $this->libJWT->putData($arr_config);
					$resp = json_decode($resp);

					if ($resp->error) {
						$this->session->setFlashdata("msg",$resp->messages);
						echo redirect()->to(base_url('consejero'));
					}
					else {
						$this->session->setFlashdata("msg",$resp->messages);
						return redirect()->to(base_url("consejero"));
					}
				}
			}
		}

		
	}

	public function foto($id) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url('login'));
		}

		$validation_rules = array('foto' => array('uploaded[foto]'));
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu' => $menu_arr));

		if ($this->session->get("jwt_token")) {
			if (!$this->request->getPostGet()) {
				$arr_config = array(
					'URI' => "/API/get/informacion_consejero",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array('id' => $id),
				);

				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);

				$data["id"] = $id;
				$data["foto_existente"] = json_decode($resp->data)->foto;
				echo view("Sistema/foto_consejero", $data);
 			}
 			else {
 				if (!$this->validate($validation_rules)) {
					$data = array('val_error'=>$this->validator->listErrors(),);
					$this->session->setFlashdata("msg",$this->validator->listErrors());
					return redirect()->to(base_url('consejero/foto/'.$id));
 					
 				}
 				else {
 					$id = limpiarInputString($this->request->getPostGet("id"));
 					$upload_path = ROOTPATH . "/writable/tmp/fotos/".$id;
 					$final_path = ROOTPATH . "/public/uploads/fotos/".$id;
 					$foto_doc = $this->request->getFile("foto");

 					if ($foto_doc->move($upload_path)) {

 						$image = \Config\Services::image()
 								->withFile($upload_path."/".$foto_doc->getName())
 								->fit(150,200,'center')
 								->save($final_path."/".$foto_doc->getName());
 								
 						$arr_config = array(
 							'URI' => "/API/upd/foto",
 							'auth' => $this->session->get("jwt_token"),
 							'method' => 'GET',
 							'args' => array(
 								'id' => $id,
 								'foto' => "/public/uploads/fotos/".$id."/".$foto_doc->getName(),	
 							),
 						);

 						$resp = $this->libJWT->putData($arr_config);
 						$resp = json_decode($resp);

 						$this->session->setFlashdata("msg",$resp->messages);
 						return redirect()->to(base_url('consejero/foto/'.$id));
 					}
 					else {
 						$this->session->setFlashdata("msg","Error al subir la fotografía");
 						return redirect()->to(base_url("consejero/foto/".$id));
 					}
 				}
 			}
 		}
		
	}
}
