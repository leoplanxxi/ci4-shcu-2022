<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

use App\Models\SesionModel;

class Sandbox extends BaseController
{

    public function index()
    {	
    
        return view('welcome_message');
    }

	public function asistencia() {
		$sM = new SesionModel();

		$json = $sM->genAsistencia();
		$sesiones = $sM->where("activo",1)->findAll();
		$data = [
				'asistencia' => $json,
			];
		foreach ($sesiones as $s) {
			$id = $s["id_sesion"];
			$sM->update($id,$data);
		}		
	}

	public function asistenciaS() {
		$sM = new SesionModel();

		$sM->setDataAsistencia(1,1,200914922,['asistencia' => 1]);
	}
    public function encodeBcrypt() {
    	// PASS: Shcu2022._!!   shcu2022
    	$text = $this->request->getGetPost("texto");

    	if ($text !== NULL)  {
    		echo password_hash($text,PASSWORD_BCRYPT);
    	}
    	else {
    		echo "error";
    	}
	}

	public function test() {
	var_dump($this->request->getGet());
		foreach ($this->request->getGet() as $key=>$val) {
			echo $key . "=>" .$val;
			echo "<br>";
		}
	}

	public function upload() {
		$session = session(); 

		$libJWT = new libJWT();
		$libHCU = new libHCU();
		
		$jwt = $libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$session->set(['jwt_token' => $token]);

		helper(['url','form']);
		$validation_rules=array('nombre'=>'required','descripcion'=>'required');

		$menu_arr = $libHCU->menuRol('consejero');

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if (!$this->request->getPostGet()) {
			
			echo view("Sistema/solicitudgestion_crear_consejero.php");
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				echo view("Sistema/solicitudgestion_crear_consejero.php",$data);
				 }
			else {
				if ($session->get("jwt_token")) {
					
					$arr_config = array(
						'URI' => "/API/put/solicitud_consejero",
						'auth' => $session->get("jwt_token"),
						'args' => array(
							'nombre' => $this->request->getPostGet("nombre"),
							'descripcion' => $this->request->getPostGet("descripcion"),
							'descripcion' => $this->request->getPostGet("descripcion"),
						),	
					);

					$resp = $libJWT->postData($arr_config);
					$resp=json_decode($resp);

					$data = array('val_error' => $resp->messages);

					echo view("sandbox", $data);
					
					
				}
			}
		}
		
		//return view('uploadView');
	}
	

}
