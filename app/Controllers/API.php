<?php
/* Fuente: https://onlinewebtutorblog.com/codeigniter-4-restful-apis-with-jwt-authentication/ */

// TODO: Pasar a POST las peticiones, en Get agregar soporte paginador

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use Exception;
use \Firebase\JWT\JWT;

use App\Models\AcuseModel;
use App\Models\ArchivoMaterialSesionModel;
use App\Models\AsistenciaModel;
use App\Models\BitacoraDescargasModel;
use App\Models\CargoConsejeroModel;
use App\Models\ComisionesConsejerosModel;
use App\Models\ComisionesModel;
use App\Models\DAUAModel;
use App\Models\InformacionConsejeroModel;
use App\Models\MiscModel;
use App\Models\RolesModel;
use App\Models\SesionModel;
use App\Models\SolicitudesModel;
use App\Models\TipoConsejeroModel;
use App\Models\UsuariosAdminModel;
use App\Models\UsuariosConsejerosModel;

use App\Libraries\libJWT;

class API extends ResourceController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}
		helper('securityHCU');
	}

	private function getKey() {
		return "aHJK-:E[(UI:Xj8+YESukAJrjJ*AP1s";
	}

	public function issueKey() { //hay que protegerlo mas
		 $key = $this->getKey();

         $iat = time(); // current timestamp value
         $nbf = $iat + 10;
         $exp = $iat + 3600;

         $payload = array(
             "iss" => "DCyTIC BUAP",
             "aud" => "HCU",
             "iat" => $iat, // issued at
             "nbf" => $nbf, //not before in seconds
             "exp" => $exp, // expire time in seconds
 //            "data" => $userdata,
         );

         $token = JWT::encode($payload, $key);

         $response = [
             'status' => 200,
             'error' => false,
             'messages' => 'Token OK',
             'data' => [
                 'token' => $token
             ]
         ];
         return $this->respondCreated($response);
	}
	
    public function get($verb)
    {
    	$libJWT = new libJWT();
    	
    	$key = $this->getKey();
    	$authHeader = $this->request->getHeader("Authorization");
    	
    	if ($authHeader !== NULL){ $authHeader = $authHeader->getValue(); }
    	else {
    		$response = $libJWT->createResponse(401,true,'No hay token',[]);
    		return $this->respondCreated($response);
    	}
    	
    	$token = $authHeader;

		try {
    		$decoded = JWT::decode($token,$key,array('HS256'));

    		if ($decoded) {
    			$get = $this->request->getGet();
   				switch ($verb) {
   						case 'acuses':
   							$acuseModel = new AcuseModel();

   							$acuses = $acuseModel->where("activo",1)->findAll();
   							$json = json_encode($acuses);
   							break;
    					case 'roles':
    						$rolesModel = new RolesModel();
    					
			    			$roles = $rolesModel->where("activo",1)->findAll();
			    			$json = json_encode($roles);
    						break;
    						
    					case 'archivos_material_sesion':
	    					$archivoMaterialModel = new ArchivoMaterialSesionModel();

	    					if (array_key_exists('id',$get))
								$archivos = $archivoMaterialModel->where("activo",1)->find($get["id"]);

							else if (array_key_exists('sesion',$get))
								$archivos = $archivoMaterialModel->where("activo",1)->where("sesion",$get["sesion"])->findAll();
							else 
								$archivos = $archivoMaterialModel->where("activo",1)->findAll();
											
							$json = json_encode($archivos);
    						break;
    						
						case 'asistencias':
							$asistenciasModel = new AsistenciaModel();
							
							if (array_key_exists('sesion',$get) && array_key_exists('usuario',$get))
								$asistencia = $asistencia->where('id_sesion',$get["sesion"])->where('id_usuario',$get["usuario"])->findAll();
							else if (array_key_exists('sesion',$get))
								$asistencia = $asistencia->where('id_sesion',$get["sesion"])->findAll();
							else if (array_key_exists('usuario',$get))
								$asistencia = $asistencia->where('id_usuario',$get["usuario"])->findAll();
							$json = json_encode($asistencia);
							
							break;
						case 'bitacoraDescargas':
							$bitacoraDescargasModel = new BitacoraDescargasModel();

							if (array_key_exists('usuario',$get))
								$descargas = $bitacoraDescargasModel->where('id_usuario_consejeros',$get["usuario"])->findAll();
							else if (array_key_exists('material',$get))
								$descargas = $bitacoraDescargasModel->where('id_material_sesion',$get["material"])->findAll();
							else
								$descargas = $bitacoraDescargasModel->findAll();

							$json = json_encode($descargas);
							
							break;
						case 'cargo':
							$cargosModel = new CargoConsejeroModel();

							if (array_key_exists('id',$get))
								$cargo = $cargosModel->where('activo',1)->find($get["id"]);
							else
								$cargo = $cargosModel->where('activo',1)->findAll();

							$json = json_encode($cargo);
							break;
						case 'comisiones':
							$comisionesModel = new ComisionesModel();

							if (array_key_exists('id',$get))
								$comision = $comisionesModel->where('activo',1)->find($get["id"]);
							else 
								$comision = $comisionesModel->where('activo',1)->findAll();

							$json = json_encode($comision);
							break;
						case 'comisiones_consejeros':
							$comisionesConsejerosModel = new ComisionesConsejerosModel();
							if (array_key_exists('consejero',$get) && array_key_exists('comision',$get))
								$comisiones = $comisionesConsejerosModel->where('activo',1)->where('id_consejero',$get["consejero"])->where('id_comision',$get["comision"])->findAll();
							else if (array_key_exists('consejero',$get))
								$comisiones = $comisionesConsejerosModel->where('id_consejero',$get["consejero"])->findAll();
							else if (array_key_exists('comision',$get))
								$comisiones = $comisionesConsejerosModel->where('id_comision',$get["comision"])->findAll();

							$json = json_encode($comisiones);
							break;
						case 'daua':
							$dauaModel = new DAUAModel();

							if (array_key_exists("id",$get))
								$daua = $dauaModel->where("activo",1)->find($get["id"]);
							else
								$daua = $dauaModel->where("activo",1)->findAll();

							$json = json_encode($daua);
							break;

						case 'informacion_consejero':
							$informacionModel = new InformacionConsejeroModel();
							if (array_key_exists('id',$get))
								$informacion = $informacionModel->find($get["id"]);

							$json = json_encode($informacion);
							break;

						case 'sesion':
							$sesionModel = new SesionModel();

							if (array_key_exists('id',$get))
								$sesion = $sesionModel->where('activo',1)->find($get["id"]);
							else 
								$sesion = $sesionModel->where('activo',1)->findAll();

							$json = json_encode($sesion);
							break;
							
						case 'solicitud':
							$solicitudModel = new SolicitudesModel();

							if (array_key_exists('id',$get))
								$solicitud = $solicitudModel->where('activo',1)->find($get["id"]);
							else 
								$solicitud = $solicitudModel->where('activo',1)->findAll();

							$json = json_encode($solicitud);
							break;

						case 'tipo':
							$tipoModel = new TipoConsejeroModel();

							if (array_key_exists('id',$get))
								$tipo = $tipoModel->where('activo',1)->find($get["id"]);
							else
								$tipo = $tipoModel->where('activo',1)->findAll();

							$json = json_encode($tipo);
							break;

						case 'usuarios_admin':
							$usuariosAdminModel = new UsuariosAdminModel();

							if (array_key_exists('id',$get))
								$usuario = $usuariosAdminModel->where('activo',1)->find($get["id"]);
							elseif (array_key_exists('usuario',$get))
								$usuario = $usuariosAdminModel->where("activo",1)->where("usuario",$get["usuario"])->findAll();
							elseif (array_key_exists('usuario',$get) && array_key_exists('passwd',$get))
								$usuario = $usuariosAdminModel->where('activo',1)->where('usuario',$get["usuario"])->where('passwd',$get["passwd"])->findAll();
							else
								$usuario = $usuariosAdminModel->where('activo',1)->findAll();

							$json = json_encode($usuario);
							break;

						case 'usuarios_consejeros':
							$usuariosConsejerosModel = new UsuariosConsejerosModel();

							if (array_key_exists('id',$get))
								$usuario = $usuariosConsejerosModel->where('activo',1)->find($get["id"]);
							else if (array_key_exists('id',$get) && array_key_exists('passwd',$get))
								$usuario = $usuariosConsejerosModel->where('activo',1)->where('id',$get["id"])->where('passwd',$get["passwd"]);
							else 
								$usuario = $usuariosConsejerosModel->where('activo',1)->findAll();

							$json = json_encode($usuario);
							break;
						case 'aviso_sesion':
							$miscModel = new MiscModel();

							$id_db=1;
							$aviso = $miscModel->find($id_db);

							$json = json_encode($aviso);
							break;
						case 'aviso_sesion_activa':
							$miscModel = new MiscModel();
							$id_db=3;
							$sesion = $miscModel->find($id_db);
							$json = json_encode($sesion);
							break;
						default:
   							return $this->respondCreated($libJWT->createResponse(405,true,'Error',[]));
   							break;
    				}
    				
    			
    			
				$response = $libJWT->createResponse(200,false,$verb,$json);

    			return $this->respondCreated($response);
    		}
    		
    	}
    	catch (Exception $ex) {
    		$response = $libJWT->createResponse(401,true,'Acceso denegado',[]);
  
  			return $this->respondCreated($response);
    	}	
	
    	
    }

    public function put($verb) {
    	$libJWT = new libJWT();
    	
    	$key = $this->getKey();
	   	$authHeader = $this->request->getHeader("Authorization");
	   	
	   	if ($authHeader !== NULL){ $authHeader = $authHeader->getValue(); }
	   	else {
	   		$response = $libJWT->createResponse(401,true,'No hay token',[]);
	   		return $this->respondCreated($response);
	   	}
	   	
	   	$token = $authHeader;

	   	try{
   			$decoded = JWT::decode($token,$key,array('HS256'));
   	
    		if ($decoded) {
    			switch($verb) {
    				case 'misc':
    					$miscModel = new MiscModel();

    					$llave = $this->request->getPost("llave");
    					$valor = $this->request->getPost("valor");

    					$data = [
    						'llave' => 'hola',
    						'valor' => 'hola',
    					];

    					if ($miscModel->insert($data)) {
    						$response = $libJWT->createResponse(200,false,"Inserción exitosa",[]);
    					}
    					else {
    						$response = $libJWT->createResponse(401,true,"Error de inserción",[]);
    					}
    					break;
    				case 'solicitud_consejero':
    					$solicitudModel = new SolicitudesModel();

    					$data = [
    						'nombre' => limpiarInputString($this->request->getPostGet("nombre")),
    						'descripcion' => limpiarInputString($this->request->getPostGet("descripcion")),
							'fecha' => limpiarInputString($this->request->getPostGet("fecha")), //Como sanitizar fechas							
							'id_usuario' => limpiarInputString($this->request->getPostGet("id_usuario")),    							
    					];

    					if ($solicitudModel->insert($data)) {
    						$response = $libJWT->createResponse(200,false,"Inserción exitosa",['insert_id' => $solicitudModel->getInsertID(),]);
    					}
	    				else { $response = $libJWT->createResponse(401,true,"Error de inserción",[]); }
						break;
					case 'sesion':
						$sesionModel = new SesionModel();

						$data = [
							'nombre' => limpiarInputString($this->request->getPostGet("nombre")),
							'fecha' => limpiarInputString($this->request->getPostGet("fecha")),	
							'asistencia' => $sesionModel->genAsistencia(),
						];
						if ($sesionModel->insert($data)) {
    						$response = $libJWT->createResponse(200,false,"Inserción exitosa",['insert_id' => $sesionModel->getInsertID(),]);
    					}
	    				else { $response = $libJWT->createResponse(401,true,"Error de inserción",[]); }
						break;

					case 'archivo_sesion':
						$archivoSModel = new ArchivoMaterialSesionModel();

						$data = [
							'nombre' => limpiarInputString($this->request->getPostGet("nombre")),
							'descripcion' => limpiarInputString($this->request->getPostGet("descripcion")),
							'archivo' => $this->request->getPostGet("archivo"),
							'sesion' => limpiarInputString($this->request->getPostGet("sesion")),
						];
						if ($archivoSModel->insert($data)) $response = $libJWT->createResponse(200,false,"Inserción exitosa", ['insert_id' => $archivoSModel->getInsertID()]);
						else $response = $libJWT->createResponse(401,true,"Error de inserción",[]);
						break;
					case 'descargar_archivo':
						$bitacoraModel = new BitacoraDescargasModel();

						$data = [
							'id_usuario_consejeros' => limpiarInputString($this->request->getPostGet("id_usuario_consejeros")),
							'id_material_sesion' => limpiarInputString($this->request->getPostGet("id_material_sesion")),	
						];
						
						if ($bitacoraModel->insert($data)) $response = $libJWT->createResponse(200,false,"Inserción exitosa", ['insert_id' => $bitacoraModel->getInsertID()]);
						else $response = $libJWT->createResponse(401,true,"Error de inserción",[]);
						break;
    			}
    		}
    		return $this->respondCreated($response);
	   	}
	   	catch (Exception $ex) {
   			$response = $libJWT->createResponse(401,true,'Acceso denegado',[]);
  
  			return $this->respondCreated($response);
	   	}
    	
    }

	public function getView($verb) {
       	$libJWT = new libJWT();
        	
       	$key = $this->getKey();
   	   	$authHeader = $this->request->getHeader("Authorization");
   	   	
   	   	if ($authHeader !== NULL){ $authHeader = $authHeader->getValue(); }
   	   	else {
   	   		$response = $libJWT->createResponse(401,true,'No hay token',[]);
   	   		return $this->respondCreated($response);
   	   	}
   	   	
   	   	$token = $authHeader;
   
   	   	try{
   	   		$decoded = JWT::decode($token,$key,array('HS256'));

       		if ($decoded) {
       			$get = $this->request->getGet();
  				switch($verb) {
  					case 'bitacora_descargas':
  						$bitacoraModel = new BitacoraDescargasModel();

						$id_material = $this->request->getPostGet("id_material");
  						$downs = $bitacoraModel->getDescargasbyArchivo($id_material);

  						$json = json_encode($downs);
  						break;
  					case 'solicitudes':
  						$solicitudModel = new SolicitudesModel();
  						if (!array_key_exists("id_usuario", $get)) {
	  						$sols = $solicitudModel->getSolicitudes();
  						}
  						else {
  							$sols = $solicitudModel->getSolicitudes($get["id_usuario"]);
  						}
  						
  						$json = json_encode($sols);
  						break;
  					case 'solicitud':
  						$solicitudModel = new SolicitudesModel();

  						$sol = $solicitudModel->getSolicitudbyID($get["id_solicitud"]);

  						$json = json_encode($sol);
  						break;
  					case 'sesiones':
  						$sesionModel = new SesionModel();

  						$ss = $sesionModel->getSesionesOrderbyFecha();

  						$json = json_encode($ss);
  						break;
  					case 'archivos_sesion':
  						$archivoSModel = new ArchivoMaterialSesionModel();

  						$material = $archivoSModel->getMaterialbySesion($get["id_sesion"]);
  						$json = json_encode($material);
  						break;
  					case 'consejeros_ua':
  						$consejerosModel = new InformacionConsejeroModel();

						if ($this->request->getPostGet("id_daua")) {
							$id_daua = limpiarInputString($this->request->getPostGet("id_daua"));
	  						$consejeros = $consejerosModel->getConsejerosbyDAUA_grouped($id_daua);	
						} else {
							$consejeros = $consejerosModel->getallConsejeros();
						}
  						
  						$json = json_encode($consejeros);
  						break;
  					case 'asistencias_ua':
  						$sesionModel = new SesionModel();

  						$id_sesion = limpiarInputString($this->request->getPostGet("id_sesion"));
  						if ($this->request->getPostGet("id_daua")) {
  							$id_daua = $this->request->getPostGet("id_daua");
  							$asistencia = $sesionModel->getListaAsistenciabyDAUA($id_sesion,$id_daua);
  						} else {
	  						$asistencia = $sesionModel->getListaAsistencia($id_sesion);
  						}
						$json = json_encode($asistencia);
  						break;
  					case 'expediente':
  						$informacionModel = new InformacionConsejeroModel();

  						$id_usr = limpiarInputString($this->request->getPostGet("id"));
  						$expediente = $informacionModel->getExpediente($id_usr);

  						$json = json_encode($expediente);
  						break;
  					
  				}
  			}
  			$response = $libJWT->createResponse(200,false,$verb,$json);
       		return $this->respondCreated($response);
   			}
   		catch (Exception $ex) {
   			$response = $libJWT->createResponse(401,true,'Acceso denegado',[]);
   			     
			return $this->respondCreated($response);
   		}
	}
   			
 	public function upd($verb) {
 		if (!function_exists('str_starts_with')) {
		    function str_starts_with($haystack, $needle) {
		        return (string)$needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0;
		    }
		}
       	$libJWT = new libJWT();
        	
       	$key = $this->getKey();
   	   	$authHeader = $this->request->getHeader("Authorization");
   	   	
   	   	if ($authHeader !== NULL){ $authHeader = $authHeader->getValue(); }
   	   	else {
   	   		$response = $libJWT->createResponse(401,true,'No hay token',[]);
   	   		return $this->respondCreated($response);
   	   	}
   	   	
   	   	$token = $authHeader;
   
   	   	try{
   			$decoded = JWT::decode($token,$key,array('HS256'));
   
       		if ($decoded) {
       			switch($verb) {
       				case 'informacion_consejero':
       					$informacionModel = new InformacionConsejeroModel();

       					$id = limpiarInputString($this->request->getPostGet("id"));

       					$data = [
       						'grado' => limpiarInputString($this->request->getPostGet("grado")),	
       						'cumpleanos' => $this->request->getPostGet("cumpleanos"),	
       						'tel_cel_1' => limpiarInputString($this->request->getPostGet("tel_cel_1")),	
       						'tel_cel_1_whats' => limpiarInputString($this->request->getPostGet("tel_cel_1_whats")),	       						
       						'tel_cel_2' => limpiarInputString($this->request->getPostGet("tel_cel_2")),	
       						'tel_cel_2_whats' => limpiarInputString($this->request->getPostGet("tel_cel_2_whats")),
       					    'tel_casa' => limpiarInputString($this->request->getPostGet("tel_casa")),	
       					    'tel_oficina' => limpiarInputString($this->request->getPostGet("tel_oficina")),	
       					    'extension' => limpiarInputString($this->request->getPostGet("extension")),	
       					    'correo_institucional' => limpiarInputString($this->request->getPostGet("correo_institucional")),	
       					    'correo_personal' => limpiarInputString($this->request->getPostGet("correo_personal")),	
       					    'comision' => limpiarInputString($this->request->getPostGet("comision")),	
       					    'protesta' => limpiarInputString($this->request->getPostGet("protesta")),	

       					];

       					if ($informacionModel->update($id,$data)) {
       						$response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
       					}
       					else {
       						$response = $libJWT->createResponse(401,true,"Error de actualización",[]);
       					}
       					break;
       				case 'foto':
       					$informacionModel = new InformacionConsejeroModel();

       					$id = limpiarInputString($this->request->getPostGet("id"));

       					$data = [ 'foto' => $this->request->getPostGet("foto"),];

       					if ($informacionModel->update($id,$data)) {
       						$response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
       					}
       					else {
       						$response = $libJWT->createResponse(401,true,"Error de actualización",[]);
       					}
       					
       					break;
       				case 'solicitud_archivo_consejero':
       					$solicitudModel = new SolicitudesModel();

       					$id = limpiarInputString($this->request->getPostGet("id_solicitud"));
   
       					$data = [ 
       						'archivo' => limpiarInputString($this->request->getPostGet("archivo")),   							
       					];
   
       					if ($solicitudModel->update($id,$data)) {
       						$response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
       					}
   	    				else { $response = $libJWT->createResponse(401,true,"Error de actualización",[]); }
   						break;
   					case 'solicitud_enlace':
   						$solicitudModel = new SolicitudesModel();

   						$id = limpiarInputString($this->request->getPostGet("id_solicitud"));

   						$data = [
   							'acuse_idacuse' => limpiarInputString($this->request->getPostGet("acuse_idacuse")),
   							'estado' => limpiarInputString($this->request->getPostGet("estado")),
   							'respuesta' => limpiarInputString($this->request->getPostGet("respuesta")),
   							'detalle_respuesta' => limpiarInputString($this->request->getPostGet("detalle_respuesta")),
   						];

   						if ($solicitudModel->update($id,$data)) $response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
   						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
   						break;
   					case 'cerrar_solicitud_secretario':
   						$solicitudModel = new SolicitudesModel();
   						$id = limpiarInputString($this->request->getPostGet("id_solicitud"));

   						$data = [ 'cerrado' => 1 ];
   						if ($solicitudModel->update($id,$data)) $response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
   						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
   						
   						break;
   					case 'asistencia_sesion':
   						$get = $this->request->getGet();
   						$sesionModel = new SesionModel();
   						$data = [];

   						$id_sesion = limpiarInputString($this->request->getPostGet("id_sesion"));
   						$id_daua = limpiarInputString($this->request->getPostGet("id_daua"));
   						$id = limpiarInputString($this->request->getPostGet("id")); #Matricula

   						if (array_key_exists('asistencia',$get)) $data['asistencia'] = $get["asistencia"];
   						if (array_key_exists('propietario', $get)) $data['propietario'] = $get["propietario"];
   						if (array_key_exists('resp', $get)) $data['resp'] = $get["resp"];
   						if (array_key_exists('voto', $get)) $data['voto'] = $get["voto"];
   						if (array_key_exists('justificante', $get)) $data['justificante'] = $get["justificante"];

   						if ($sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,$data)) $response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
   						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
   						
   						break;
   					case 'asistencia_sesion_bulk':   						
   						$sesionModel = new SesionModel();
   						$request = $this->request->getGetPost("request");
   						$id_sesion = limpiarInputString($this->request->getGetPost("id_sesion"));
   						$err = false;
   						foreach ($request as $key=>$value) {
   							if (str_starts_with($key,"voto")) {
   								$str = explode("_",$key);
   								$id_daua = $str[1];
   								$id = $str[2];
   								if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,['voto' => $value])) $err = true;
   							}
   							if (str_starts_with($key,"asistencia")) {
   								$str = explode("_",$key);
   								$id_daua = $str[1];
   								$id = $str[2];
   								if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,['asistencia' => $value])) $err = true;
   							}
   						}

   						if (!$err) $response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
   						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
   						
   						break;
   					case 'asistencia_sesion_secr_bulk':
   						$sesionModel = new SesionModel();
   						$informacionModel = new InformacionConsejeroModel();
   						$request = $this->request->getGetPost("request");
   						$id_sesion = limpiarInputString($this->request->getGetPost("id_sesion"));
   						$err = false;
   						foreach ($request as $key=>$value){
   							if (str_starts_with($key,"asistencia")) {
   								$str = explode("_",$value);
   								
   								$id = $str[1];
								$res = $str[0];

								$info = $informacionModel->where("activo",1)->find($id);
								$id_daua = $info["daua"];
   								$asoc = $info["consejero_asociado"];
								if ($res != "ninguno") {
									if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,['resp' => 1])) $err = true;
									if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$asoc,['resp' => 0])) $err = true;
									
								}
								else {
									if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,['resp' => 0])) $err = true;
									if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$asoc,['resp' => 0])) $err = true;	
								}
   								
   							}	
   						}

						if ($this->request->getGetPost("archivo")) {
							//REVISAR ESTO
							$id = $request["id_mat"];
							if (!$sesionModel->setDataAsistencia($id_sesion,$id_daua,$id,['justificante' => $this->request->getGetPost("archivo")])) $err = true;
						}
   						if (!$err) $response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
   						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
   						break;
   					case 'aviso_sesion':
   						$miscModel = new MiscModel();

   						$aviso = $this->request->getPostGet("aviso"); /* ??? */
   						$sesion_activa = $this->request->getPostGet("sesion_activa");
   						$id_db = 1;
   						$data = ['valor' => $aviso];
   						
       					if ($miscModel->update($id_db,$data)) {

       						$id_db = 3;
       						$data = ['valor'=>$sesion_activa];
       						if ($miscModel->update($id_db,$data)) {
	      						$response = $libJWT->createResponse(200,false,"Actualización exitosa",[]);
       						}
       						else $response = $libJWT->createResponse(401,true,"Error de actualización",[]);
       					}
   	    				else { $response = $libJWT->createResponse(401,true,"Error de actualización",[]); }
   						break;
       			}
       		}
       		return $this->respondCreated($response);
   	   	}
   	   	catch (Exception $ex) {
   	   	var_dump($ex);
      			$response = $libJWT->createResponse(401,true,'Acceso denegado',[]);
     
     			return $this->respondCreated($response);
   	   	}
       	
   }  

   public function softDel($verb) {
   		$libJWT = new libJWT();

   		$key = $this->getKey();
   		$authHeader = $this->request->getHeader("Authorization");
   		
   	   	if ($authHeader !== NULL){ $authHeader = $authHeader->getValue(); }
   	   	else {
   	   		$response = $libJWT->createResponse(401,true,'No hay token',[]);
   	   		return $this->respondCreated($response);
   	   	}
   	   	
   	   	$token = $authHeader;
   	   	try{
   			$decoded = JWT::decode($token,$key,array('HS256'));
   
       		if ($decoded) {
       			$get  = $this->request->getGet();
       			$data = [ 
					'activo' => 0,   							
				];
       			switch($verb) {
       				
       				case 'solicitud':
       					$solicitudModel = new SolicitudesModel();

       					$id = limpiarInputString($this->request->getPostGet("id_solicitud"));		
   
       					if ($solicitudModel->update($id,$data)) {
       						$response = $libJWT->createResponse(200,false,"Eliminación exitosa",[]);
       					}
   	    				else { $response = $libJWT->createResponse(401,true,"Error de eliminación",[]); }
   						break;
   					case 'sesion':
   						$sesionModel = new SesionModel();
   						$id = limpiarInputString($this->request->getPostGet("id_sesion"));
   						if ($sesionModel->update($id,$data)) {
       						$response = $libJWT->createResponse(200,false,"Eliminación exitosa",[]);
       					}
   	    				else { $response = $libJWT->createResponse(401,true,"Error de eliminación",[]); }
   	    				break;
   	    			case 'archivo_sesion':
   	    				$archivoModel = new ArchivoMaterialSesionModel();
   	    				$id = limpiarInputString($this->request->getPostGet("id_archivo_material_sesion"));
   	    				if ($archivoModel->update($id,$data)) {
	   						$response = $libJWT->createResponse(200,false,"Eliminación exitosa",[]);
       					}
	   	    				else { $response = $libJWT->createResponse(401,true,"Error de eliminación",[]); }
    					
    					break;
       			}
       		}
       		return $this->respondCreated($response);
   	   	}
   	   	catch (Exception $ex) {
      			$response = $libJWT->createResponse(401,true,'Acceso denegado',[]);
     
     			return $this->respondCreated($response);
   	   	}
   } 
}
