<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Intro extends BaseController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session();
		 
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
			
		$this->libJWT = new libJWT();	

		$jwt = $this->libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$this->session->set(['jwt_token' => $token]);
	
		$this->libHCU = new libHCU();

		$this->rol = $this->session->get("user_info")["rol"];
	}

	public function index() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/get/aviso_sesion",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array(),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			$data["aviso"] = json_decode($resp->data)->valor;
			
			$arr_config = array(
				'URI' => "/API/get/aviso_sesion_activa",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array(),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);
			$data["sesion_activa"] = json_decode($resp->data)->valor;
			echo view("Sistema/intro",$data);

		}
	}
}
