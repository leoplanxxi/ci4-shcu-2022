<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Models\BitacoraDescargasModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Sesiones extends BaseController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session();
		 
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
			
		$this->libJWT = new libJWT();	

		$jwt = $this->libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$this->session->set(['jwt_token' => $token]);
	
		$this->libHCU = new libHCU();

		$this->rol = $this->session->get("user_info")["rol"];
	}

	public function aviso() {
		if (!$this->session->get("logged_in")) return redirect()->to(base_url("login"));

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/get/aviso_sesion",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array( ),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			$arr_config = array(
				'URI' => "/API/get/sesion",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array()
			 );
			 $resp_sesiones = $this->libJWT->putData($arr_config);
			 $resp_sesiones = json_decode($resp_sesiones); 

			 $arr_config = array(
			 	'URI' => "/API/get/aviso_sesion_activa",
			 	'auth' => $this->session->get("jwt_token"),
			 	'method' => 'GET',
			 	'args' => array(),
			 );
			 $resp_sesionactiva = $this->libJWT->putData($arr_config);
			 $resp_sesionactiva = json_decode($resp_sesionactiva);

			
			if (!$this->request->getPostGet()) {
				$data["aviso"] = json_decode($resp->data)->valor;
				$data["sesiones"] = json_decode($resp_sesiones->data);
				$data["sesion_activa"] = json_decode($resp_sesionactiva->data);
				echo view("Sistema/sesiones_aviso",$data);
			}
			else {
				$arr_config = array(
					'URI' => "/API/upd/aviso_sesion",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array('aviso' => nl2br($this->request->getPostGet("aviso")), 'sesion_activa' => limpiarInputString($this->request->getPostGet("sesion_activa"))),	
				);

				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);

				if ($resp->error) {
 					$this->session->setFlashdata("msg",$resp->messages);
 					return redirect()->to(base_url('sesiones/lista'));
 				}
 				else {
 					$this->session->setFlashdata("msg",$resp->messages);
 					return redirect()->to(base_url('sesiones/lista'));
 				} 
			}

		}
	}

	public function lista() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu'=>$menu_arr));

		if($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/sesiones",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => [],
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			if ($resp->error!=="false"){
				$data["sesiones"] = json_decode($resp->data);
				$data["rol"] = $this->rol;

				echo view("Sistema/sesiones_lista",$data);
			}
		}
	}

	

	public function crear() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$validation_rules = array('nombre' => 'required', 'fecha' => 'required|valid_date');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu' => $menu_arr));

		if (!$this->request->getPostGet()) {
			echo view("Sistema/sesiones_crear");
		} else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error'=>$this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				echo view("Sistema/sesiones_crear");
			}
			if ($this->session->get("jwt_token")) {
				$arr_config = array(
					'URI' => '/API/put/sesion',
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array(
						'nombre' => limpiarInputString($this->request->getPostGet("nombre")),					
						'fecha' => limpiarInputString($this->request->getPostGet("fecha")),				
					),
				);

				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);

				if ($resp->error) {
					$this->session->setFlashdata("msg",$resp->messages);
					return redirect()->to(base_url("sesiones/lista"));
				}
				else {
					$this->session->setFlashdata("msg",$resp->messages);
					return redirect()->to(base_url("sesiones/lista"));
				}
			}
		}
	}

	public function archivos($id_sesion) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu' => $menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/archivos_sesion?id_sesion=".$id_sesion,
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => [],
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			if ($resp->error !== "false") {
				$data["archivos"] = json_decode($resp->data);
				$data["id_sesion"] = $id_sesion;
				$data["rol"] = $this->rol;
				echo view("Sistema/sesiones_archivos",$data);
			}
		}
	}

	public function descargar($id_usuario,$id_archivo) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
	
		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/get/archivos_material_sesion/?id=".$id_archivo,
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => [],
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);
			$file = json_decode($resp->data)->archivo;

			$arr_config = array(
			 	'URI' => "/API/put/descargar_archivo",
			 	'auth' => $this->session->get("jwt_token"),
			 	'method' => 'GET',
			 	'args' => array(
			 		'id_usuario_consejeros' => $id_usuario,
			 		'id_material_sesion' => $id_archivo,	
			 	),
			);

			$resp = $this->libJWT->putData($arr_config);

			return redirect()->to(base_url($file));
		}
	}

	
	public function archivos_subir($id_sesion) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$validation_rules = array('nombre' => 'required', 'descripcion' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);
		
		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu' => $menu_arr));

		$data["id_sesion"] = $id_sesion;

		if (!$this->request->getPostGet()) {
			echo view("Sistema/sesiones_archivos_subir",$data);
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data["val_error"] = $this->validator->listErrors();
				$this->session->setFlashdata("msg", $this->validator->listErrors());
				return redirect()->to(base_url("sesiones/archivos_subir/".$data["id_sesion"]));
			
			}
			else {
				if ($this->session->get("jwt_token")) {
					$id_sesion = limpiarInputString($this->request->getPostGet("id_sesion"));
					$upload_path = ROOTPATH . "/public/uploads/material_sesiones/" . $id_sesion;

					$archivo_doc = $this->request->getFile("archivo");

					if ($archivo_doc->move($upload_path)) {
						$arr_config = array(
							'URI' => "/API/put/archivo_sesion",
							'auth' => $this->session->get("jwt_token"),
							'method' => 'GET',
							'args' => array(
								'nombre' => limpiarInputString($this->request->getPostGet("nombre")),
								'descripcion' => limpiarInputString($this->request->getPostGet("descripcion")),
								'archivo' => "/public/uploads/material_sesiones/".$id_sesion."/".$archivo_doc->getName(),
								'sesion' => limpiarInputString($this->request->getPostGet("id_sesion")),	
							),
						);

						$resp = $this->libJWT->putData($arr_config);
						$resp = json_decode($resp);

						$this->session->setFlashdata("msg",$resp->messages);
						return redirect()->to(base_url('sesiones/archivos/'.$id_sesion));
						
					}
					else {
						$this->session->setFlashdata("msg","Error al subir archivo.");
						echo view("Sistema/sesiones_archivos_subir");
					}
				
				}
			}
		}
	}
	public function eliminar_archivo($id_archivo, $id_sesion) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
			
		$validation_rules = array('id_archivo_material_sesion' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if (!$this->request->getPostGet()) {
			$data["id"] = $id_archivo;
			echo view("Sistema/sesiones_eliminar_archivo",$data);
						
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				return redirect()->to(base_url('sesiones/archivos/'.$id_sesion));
			 }
			 else {

		 		if ($this->session->get("jwt_token")) {
		 			$arr_config = array(
		 				'URI' => "/API/softDel/archivo_sesion",
		 				'auth' => $this->session->get("jwt_token"),
		 				'method' => 'GET',
		 				'args' => array(
		 					'id_archivo_material_sesion' => limpiarInputString($this->request->getPostGet("id_archivo_material_sesion")),
		 				),
		 			);

		 			$resp = $this->libJWT->putData($arr_config);
		 			$resp = json_decode($resp);

		 			$this->session->setFlashdata("msg", $resp->messages);
		 			return redirect()->to(base_url('sesiones/archivos/' . $id_sesion));
		 		}
			 }
			
		}
		
				
	}

	public function eliminar_sesion($id) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
	
		$validation_rules = array('id_sesion' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if (!$this->request->getPostGet()) {
			$data["id"] = $id;
			echo view("Sistema/sesiones_eliminar",$data);
						
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				return redirect()->to(base_url('sesiones/lista'));
			 }
			 else {

		 		if ($this->session->get("jwt_token")) {
		 			$arr_config = array(
		 				'URI' => "/API/softDel/sesion",
		 				'auth' => $this->session->get("jwt_token"),
		 				'method' => 'GET',
		 				'args' => array(
		 					'id_sesion' => limpiarInputString($this->request->getPostGet("id_sesion")),
		 				),
		 			);

		 			$resp = $this->libJWT->putData($arr_config);
		 			$resp = json_decode($resp);

		 			$this->session->setFlashdata("msg", $resp->messages);
		 			return redirect()->to(base_url('sesiones/lista'));
		 		}
			 }
			
		}
		
				
	}

	public function asistentes($id_sesion){
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$validation_rules = array('id_sesion' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/asistencias_ua",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => [
					'id_sesion' => $id_sesion,
				],
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			if (!$this->request->getPostGet()) {
				$data["asistencias"] = json_decode($resp->data);
				$data["id_sesion"] = $id_sesion;
				echo view("Sistema/sesiones_asistencia",$data);
			} else {
				if (!$this->validate($validation_rules)) {
					$data = array('val_error' => $this->validator->listErrors(),);
					$this->session->setFlashdata("msg",$this->validator->listErrors());
					return redirect()->to(base_url('sesiones/asistentes/'.$id_sesion));
				} else {
					$request = $this->request->getPostGet();
					$arr_config = array(
						'URI' => "/API/upd/asistencia_sesion_bulk",
						'auth' => $this->session->get("jwt_token"),
						'method' => 'GET',
						'args' => array(
							'id_sesion' => limpiarInputString($this->request->getGetPost("id_sesion")),
							'request' => $request,
						),
					);

					$resp = $this->libJWT->putData($arr_config);
					$resp = json_decode($resp);

					$this->session->setFlashdata("msg", $resp->messages);
		 			return redirect()->to(base_url('sesiones/asistentes/'.$id_sesion));
				}
			}
 
		
		}
	} 
 
	public function reporte_asistentes($id_sesion) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}	
		
		$validation_rules = array('id_sesion' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));
		echo view("Debug/console_div_js");

		if ($this->session->get("jwt_token")) {
			$id_daua = $this->session->get("user_info")["daua"];
			$arr_config = array(
				'URI' => "/API/getView/asistencias_ua",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array(
					'id_sesion' => $id_sesion,
					'id_daua' => $id_daua,
				),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			if (!$this->request->getPostGet()) {
				$data["id_sesion"] = $id_sesion;
				$data["asistentes"] = json_decode($resp->data);
				echo view("Sistema/sesiones_asistencia_secretarioacademico",$data);
			} else {
				if (!$this->validate($validation_rules)) {
					$data = array('val_error' => $this->validator->listErrors(),);
					$this->session->setFlashdata("msg",$this->validator->listErrors());
					return redirect()->to(base_url('sesiones/reporte_asistentes/'.$id_sesion.'/'.$id_daua));
				} else {
					$request = $this->request->getPostGet();

					if (empty($_FILES["archivo"]["name"])) {
						$arr_config = array(
							'URI' => "/API/upd/asistencia_sesion_secr_bulk",
							'auth' => $this->session->get("jwt_token"),
							'method' => 'GET',
							'args' => array(
								'id_sesion' => limpiarInputString($this->request->getGetPost("id_sesion")),
								'request' => $request,
							),
						);
	
						$resp = $this->libJWT->putData($arr_config);
						$resp = json_decode($resp);
	
						$this->session->setFlashdata("msg", $resp->messages);
			 			return redirect()->to(base_url('sesiones/reporte_asistentes/'.$id_sesion.'/'.$id_daua));		
					}
					else {
						$id_sesion = limpiarInputString($this->request->getPostGet("id_sesion"));
						$upload_path = ROOTPATH . "/public/uploads/justificantes_sesiones/" . $id_sesion."/".$id_daua;

						$archivo_doc = $this->request->getFile("archivo");

						if ($archivo_doc->move($upload_path)) {
							$arr_config = array(
								'URI' => "/API/upd/asistencia_sesion_secr_bulk",
								'auth' => $this->session->get("jwt_token"),
								'method' => 'GET',
								'args' => array(
									'id_sesion' => limpiarInputString($this->request->getPostGet("id_sesion")),
									'request' => $request,
									'archivo' => "/public/uploads/justificantes_sesiones/" . $id_sesion."/".$id_daua."/".$archivo_doc->getName(),			
								),
							);

							$resp = $this->libJWT->putData($arr_config);
							$resp = json_decode($resp);

							$this->session->setFlashdata("msg",$resp->messages);
				 			return redirect()->to(base_url('sesiones/reporte_asistentes/'.$id_sesion.'/'.$id_daua));		

						}
						else {
							$this->session->setFlashdata("msg","Error al subir archivo.");
				 			return redirect()->to(base_url('sesiones/reporte_asistentes/'.$id_sesion.'/'.$id_daua));		
						}
					}
				}
			}
		}
	}

	public function bitacora_descargas($id_material) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to("login");
		}

		$menu_arr = $this->libHCU->menuRol($this->rol);
	
			echo view("Common/common");
			echo view("Common/header");
			echo view("Common/menu",array('menu'=>$menu_arr));
	
			if ($this->session->get("jwt_token")) {
				$arr_config = array(
					'URI' => "/API/getView/bitacora_descargas",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array(
						'id_material' => $id_material,
					),
				);
	
				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);

				$data["descargas"] = json_decode($resp->data);

				echo view("Sistema/sesiones_descargas",$data);
			}
	}
 }
