<?php

namespace App\Controllers;

use App\Models\LoginModel;
use App\Models\InformacionConsejeroModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Login extends BaseController  {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session(); 

		$this->libJWT = new libJWT();
	
		$this->libHCU = new libHCU();
	}

	public function index() {
		if ($this->session->get("jwt_token")) return redirect()->to("intro");
		$validation_rules = array('matricula' => 'required', 'pass' => 'required');
		if (!$this->request->getPostGet()) {
			echo view("Common/common");
			
			echo view("Login/login");	
		} else {
			if (!$this->validate($validation_rules)) {
				$data["val_error"] = $this->validator->listErrors();
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				return redirect()->to(base_url("login"));
			}
			else {
				$loginModel = new LoginModel();
				
				$usr = $this->request->getPostGet("matricula");
				$pass = $this->request->getPostGet("pass");
	
				if (!$loginModel->checkUsrPass($usr,$pass)) {
					$this->session->setFlashdata("msg", "Usuario o contraseña incorrectos.");
					return redirect()->to(base_url("login"));
				}
				else {
					$this->session->set(['logged_in' => true]);
					$this->session->set('user_info',['usr' => $usr]);
					return redirect()->to(base_url("login/proc"));
				}	
			}
			
		}
		
	}

	public function proc() {		
		if ($this->session->get("logged_in")) {
			
			$loginModel = new LoginModel();

			$jwt = $this->libJWT->getJWT();	
			$token = json_decode($jwt)->data->token;
			$this->session->set(['jwt_token' => $token]);

			$usr = $this->session->get('user_info')['usr'];
			$tblUsr = $loginModel->verifyTableUsr($usr);
			if ($tblUsr["tbl"] == "consejero") {
				$this->session->push('user_info',['rol' => 5]);
				$this->session->push('user_info',['idtbl' => $tblUsr['idtbl']]);
				$arr_config = array(
					'URI' => "/API/get/informacion_consejero",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array('id' => $usr),
				);

				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);
				$info = json_decode($resp->data);

				$this->session->push('user_info',['nombre' => $info->nombre . " " . $info->apellido_paterno . " " . $info->apellido_materno]);

				$arr_config = array(
					'URI' => "/API/get/daua",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array('id' => $info->daua),
				);
				
				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);
				$nombre_daua = json_decode($resp->data)->nombre;
				$this->session->push('user_info',['daua' => $nombre_daua]);
				
			}
			else {
				$arr_config = array(
					'URI' => "/API/get/usuarios_admin",
					'auth' => $this->session->get("jwt_token"),
					'method' => 'GET',
					'args' => array('usuario' => $usr),	
				);

				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);
				$info = json_decode($resp->data);

				$this->session->push('user_info',['rol' => $info[0]->rol]);

				if ($info[0]->rol == 4) $this->session->push('user_info',['daua' => $info[0]->daua]);
			}
			return redirect()->to(base_url("intro"));
		} else { return redirect()->to(base_url("login")); }			
	}

	public function logout() {
		$this->session->destroy();
		return redirect()->to(base_url("login"));
	}
}
