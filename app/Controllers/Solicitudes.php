<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Solicitudes extends BaseController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session(); 

		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		
		$this->libJWT = new libJWT();
		
		$jwt = $this->libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$this->session->set(['jwt_token' => $token]);
	
		$this->libHCU = new libHCU();

		$this->rol = $this->session->get("user_info")["rol"];
	}

	public function lista() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			if ($this->rol != 5) {
				$arr_config = array(
					'URI' => "/API/getView/solicitudes",
					'auth' => $this->session->get("jwt_token"),
					'method' => "GET",
					'args' => [],
				);			
			} else {
				$arr_config = array(
					'URI' => "/API/getView/solicitudes",
					'auth' => $this->session->get("jwt_token"),
					'method' => "GET",
					'args' => ['id_usuario' => $this->session->get("user_info")["idtbl"]],
				);		
			}
		

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			if ($resp->error !== "false") {
				$data["solicitudes"] = json_decode($resp->data);
				$data["rol"] = $this->rol;

				echo view("Sistema/solicitudgestion_lista",$data);
			}
		}
		else {
			echo "Token no válido";
		}

	}

	public function crear() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		$validation_rules=array('nombre'=>'required','descripcion'=>'required','archivo'=>'uploaded[archivo]','fecha'=>['required','valid_date',],);

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if (!$this->request->getPostGet()) {
			echo view("Sistema/solicitudgestion_crear_consejero.php");
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				echo view("Sistema/solicitudgestion_crear_consejero.php");
			 }
			else {
				if ($this->session->get("jwt_token")) {
					
					$arr_config = array(
						'URI' => "/API/put/solicitud_consejero",
						'auth' => $this->session->get("jwt_token"),
						'method' => "GET",
						'args' => array(
							'nombre' => limpiarInputString($this->request->getPostGet("nombre")),
							'descripcion' => limpiarInputString($this->request->getPostGet("descripcion")),
							'fecha' => limpiarInputString($this->request->getPostGet("fecha")), //Como sanitizar fechas
							'archivo' => limpiarInputString($this->request->getPostGet("descripcion")),
							'id_usuario' => $this->session->get("user_info")["idtbl"],
						),	
					);

					$resp = $this->libJWT->putData($arr_config);

					$resp=json_decode($resp);


					if ($resp->error) {
						$this->session->setFlashdata("msg",$resp->messages);
						return redirect()->to(base_url("solicitudes/crear"));
					}
					else {
						$id_solicitud = $resp->data->insert_id;
						$msg = $resp->messages;

						$upload_path = ROOTPATH . "/public/uploads/solicitudes/".$id_solicitud;
						
						$archivo_doc = $this->request->getFile('archivo');

						//¿VALIDAR?
						if ($archivo_doc->move($upload_path)) {
							$arr_config = array(
								'URI' => "/API/upd/solicitud_archivo_consejero",
								'auth' => $this->session->get("jwt_token"),
								'method' => "GET",
								'args' => array(
									'id_solicitud' => $id_solicitud,
									'archivo' => "/public/uploads/solicitudes/".$id_solicitud."/".$archivo_doc->getName(),
								)
							);

							$resp = $this->libJWT->putData($arr_config);
							$resp = json_decode($resp);
							
							$this->session->setFlashdata("msg",$msg."<br>".$resp->messages);
							return redirect()->to(base_url("solicitudes/lista"));
						}
						else {
							$this->session->setFlashdata("msg",$msg . "<br>Error al subir archivos.");
							return redirect()->to(base_url("solicitudes/lista"));
						}
					}					
				}
			}
		}
	}


	public function cerrar($id) {
		if (!$this->session->get("logged_in")) return redirect()->to(base_url("login"));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/upd/cerrar_solicitud_secretario",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array('id_solicitud' => $id),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);


			if ($resp->error != false) $this->session->setFlashdata("msg","Cerrado exitosamente");
			else $this->session->setFlashdata("msg", $resp->error);

			return redirect()->to(base_url('solicitudes/lista'));
		}
	}

	public function detalles($id) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		$validation_rules = array('acuse_idacuse' => 'required', 'estado' => 'required', 'respuesta' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));
		if (!$this->request->getPostGet()) {
			if ($this->session->get("jwt_token")) {
				$arr_config = array(
					'URI' => "/API/getView/solicitud?id_solicitud=".$id,
					'auth' => $this->session->get("jwt_token"),
					'method' => "GET",
					'args' => [],
				);
	
				$resp = $this->libJWT->putData($arr_config);
				$resp = json_decode($resp);
	
				if ($resp->error !== "false") {
					$data["informacion"] = json_decode($resp->data);
					$data["rol"] = $this->rol;
	
					if ($data["rol"] == 3 || $data["rol"] == 2) {
	
						$arr_config = array(
							'URI' => "/API/get/acuses",
							'auth' => $this->session->get("jwt_token"),
							'method' => "GET",
							'args' => [],	
						);
	
						$resp = $this->libJWT->putData($arr_config);
						$data["acuses"] = json_decode(json_decode($resp)->data);	
					}
					echo view("Sistema/solicitudgestion_detalles",$data);
					
				}
			}
			else {
				echo "Token no válido";
			}	
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				echo view("Sistema/solicitudgestion_detalles.php");
			 }
			 else {
				if ($this->session->get("jwt_token")) {
					$arr_config = array(
						'URI' => "/API/upd/solicitud_enlace",
						'auth' => $this->session->get("jwt_token"),
						'method' => 'GET',
						'args' => array(
							'acuse_idacuse' => limpiarInputString($this->request->getPostGet("acuse_idacuse")),
							'estado' => limpiarInputString($this->request->getPostGet("estado")),
							'respuesta' => limpiarInputString($this->request->getPostGet("respuesta")),
							'detalle_respuesta' => limpiarInputString($this->request->getPostGet("detalle_respuesta")),	
							'id_solicitud' => limpiarInputString($this->request->getPostGet("id_solicitud")),
						),
					); 

					$resp = $this->libJWT->putData($arr_config);
					$resp = json_decode($resp);

					$this->session->setFlashdata("msg",$resp->messages);

					return redirect()->to(base_url('solicitudes/lista'));
				}
			 	
			 }
		}
		
	}

	public function eliminar($id) {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		
		$validation_rules = array('id_solicitud' => 'required');
		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if (!$this->request->getPostGet()) {
			$data["id"] = $id;
			echo view("Sistema/solicitudgestion_eliminar",$data);
						
		}
		else {
			if (!$this->validate($validation_rules)) {
				$data = array('val_error' => $this->validator->listErrors(),);
				$this->session->setFlashdata("msg",$this->validator->listErrors());
				return redirect()->to(base_url('solicitudes/lista'));
			 }
			 else {

		 		if ($this->session->get("jwt_token")) {
		 			$arr_config = array(
		 				'URI' => "/API/softDel/solicitud",
		 				'auth' => $this->session->get("jwt_token"),
		 				'method' => 'GET',
		 				'args' => array(
		 					'id_solicitud' => limpiarInputString($this->request->getPostGet("id_solicitud")),
		 				),
		 			);

		 			$resp = $this->libJWT->putData($arr_config);
		 			$resp = json_decode($resp);

		 			$this->session->setFlashdata("msg", $resp->messages);
		 			return redirect()->to(base_url('solicitudes/lista'));
		 		}
			 }
			
		}
		
				
	}
}
