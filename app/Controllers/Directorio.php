<?php

namespace App\Controllers;

use App\Models\RolesModel;
use App\Models\InformacionConsejeroModel;
use App\Libraries\libJWT;
use App\Libraries\libHCU;

class Directorio extends BaseController
{
	public function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		header("Access-Control-Allow-Methods: GET, POST");
		$method = $_SERVER['REQUEST_METHOD'];
		setlocale(LC_ALL, "es_MX");
		date_default_timezone_set('America/Mexico_City');
		if ($method == "OPTIONS") {
			die();
		}

		helper(['url','form', 'securityHCU']);
		$this->session = session(); 

		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}
		
		$this->libJWT = new libJWT();
		
		$jwt = $this->libJWT->getJWT();
		$token = json_decode($jwt)->data->token;
		$this->session->set(['jwt_token' => $token]);
	
		$this->libHCU = new libHCU();

		$this->rol = $this->session->get("user_info")["rol"];
	}

	public function lista() {
		if (!$this->session->get("logged_in")) {
			return redirect()->to(base_url("login"));
		}

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu",array('menu'=>$menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/consejeros_ua",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => [],
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			$data["consejeros"] = json_decode($resp->data);

			echo view("Sistema/directorio",$data);
		}
	}

	public function expediente($id) {

		if (!$this->session->get("jwt_token")) {
			return redirect()->to(base_url("login"));
		}

		$menu_arr = $this->libHCU->menuRol($this->rol);

		echo view("Common/common");
		echo view("Common/header");
		echo view("Common/menu", array('menu' => $menu_arr));

		if ($this->session->get("jwt_token")) {
			$arr_config = array(
				'URI' => "/API/getView/expediente",
				'auth' => $this->session->get("jwt_token"),
				'method' => 'GET',
				'args' => array('id' => $id),
			);

			$resp = $this->libJWT->putData($arr_config);
			$resp = json_decode($resp);

			$data["expediente"] = json_decode($resp->data);

			echo view("Views/Sistema/directorio_expediente",$data);
		}
	}
}
