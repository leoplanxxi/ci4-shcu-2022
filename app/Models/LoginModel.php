<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {

	public function checkUsrPass($usr,$pass) {	
		$f = false;
		$builder_cons = $this->db->table("usuarios_consejeros");
		$builder_adm = $this->db->table("usuarios_admin");
				
		$query_cons = $builder_cons->getWhere(['id'=>$usr])->getResultArray();
		$query_adm = $builder_adm->getWhere(['usuario'=>$usr])->getResultArray();
		
		if (count($query_cons) > 0) {
			if (password_verify($pass,$query_cons[0]["passwd"])) $f = true;
		} else {
			if (count($query_adm) > 0) {
				if (password_verify($pass,$query_adm[0]["passwd"])) $f = true;
			}	
		}
		return $f;
		
	}

	public function verifyTableUsr($usr) {
		$builder_cons = $this->db->table("usuarios_consejeros");
		$builder_adm = $this->db->table("usuarios_admin");
				
		$query_cons = $builder_cons->getWhere(['id'=>$usr])->getResultArray();
		$query_adm = $builder_adm->getWhere(['usuario'=>$usr])->getResultArray();

		if (count($query_cons) > 0) {
			$data["tbl"] = "consejero";
			$data["idtbl"] = $query_cons[0]["id_usuarios_consejeros"];
			return $data;			
		} 
		else if (count($query_adm) > 0) {
			$data["tbl"] = "admin";
			return $data;
		}
		else { return false; }
	}
}
