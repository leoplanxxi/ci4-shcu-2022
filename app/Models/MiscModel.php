<?php namespace App\Models;

use CodeIgniter\Model;

class MiscModel extends Model {
	protected $table = "misc";
	protected $primaryKey = "id_misc";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

   	protected $allowedFields = ['id_misc','llave','valor'];

}
