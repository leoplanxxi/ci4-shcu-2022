<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\InformacionConsejeroModel;
use App\Models\UsuariosConsejerosModel;

class BitacoraDescargasModel extends Model {
	protected $table = "bitacora_descargas";
	protected $primaryKey = "id_bitacora";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['id_usuario_consejeros','id_material_sesion'];

	public function getDescargasbyArchivo($id_material_sesion) {
		$informacionModel = new InformacionConsejeroModel();
		$usuariosModel = new UsuariosConsejerosModel();
		
		$res = $this->where("id_material_sesion",$id_material_sesion)->findAll();

		$i=0;
		foreach ($res as $r) {
			$id_usr = $usuariosModel->where("activo",1)->find($r["id_usuario_consejeros"])["id"];
			$info = $informacionModel->where("activo",1)->find($id_usr);
			$res[$i]["usuario"] = $info["nombre"] . " ". $info["apellido_paterno"]. " ".$info["apellido_materno"];
			$res[$i]["id"] = $id_usr;
		
			$i++;
		}
		return $res;
	}
}
