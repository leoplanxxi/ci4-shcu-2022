<?php namespace App\Models;

use CodeIgniter\Model;

class AcuseModel extends Model {
	protected $table = "acuse";
	protected $primaryKey = "id_acuse";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','activo'];

}
