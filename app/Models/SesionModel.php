<?php namespace App\Models;

use CodeIgniter\Model;

use App\Models\DAUAModel;
use App\Models\InformacionConsejeroModel;
use App\Models\CargoConsejeroModel;
use App\Models\TipoConsejeroModel;

class SesionModel extends Model {
	protected $table = "sesion";
	protected $primaryKey = "id_sesion";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','fecha','activo','asistencia'];

	public function getSesionesOrderbyFecha() {
		return $this->where("activo",1)->orderBy('fecha','DESC')->findAll();
	}

	public function getListaAsistencia($id_sesion) {
		$dauaModel = new DAUAModel();
		$cargoModel = new CargoConsejeroModel();
		$informacionModel = new InformacionConsejeroModel();	
		$tipoModel = new TipoConsejeroModel();


		$lista = $this->where("activo",1)->find($id_sesion)["asistencia"];
		$lista = json_decode($lista);

		foreach ($lista as $key_f => $val_f) {
			$id_daua = $key_f;
			foreach ($val_f as $key_c => $val_c) {
				$id = $key_c;
				$nombre_data = $informacionModel->where("activo",1)->find($id);
				$nombre = $nombre_data["nombre"] . " " . $nombre_data["apellido_paterno"] . " " . $nombre_data["apellido_materno"];

				$lista->$id_daua->$id->id = $id;
				$lista->$id_daua->$id->id_daua = $id_daua;
				$lista->$id_daua->$id->daua = $dauaModel->where("activo",1)->find($id_daua)["nombre"];			
				$lista->$id_daua->$id->tipo = $tipoModel->where("activo",1)->find($nombre_data["tipo"])["nombre"];
				$lista->$id_daua->$id->cargo = $cargoModel->where("activo",1)->find($nombre_data["cargo"])["nombre"];
				$lista->$id_daua->$id->nombre = $nombre;
			}
			
		}

		return $lista;
	}

	public function getListaAsistenciabyDAUA($id_sesion,$id_daua) {
		$informacionModel = new InformacionConsejeroModel();
		$cargoModel = new CargoConsejeroModel();
		$tipoModel = new TipoConsejeroModel();

		$consejeros = $this->where("activo",1)->find($id_sesion)["asistencia"];
		$consejeros = json_decode($consejeros);

		$res = $consejeros->$id_daua;

		foreach ($res as $key_c=>$val_c) {
			$id = $key_c;
			$nombre_data = $informacionModel->where("activo",1)->find($id);
			$nombre = $nombre_data["nombre"] . " " . $nombre_data["apellido_paterno"] . " " . $nombre_data["apellido_materno"];

			$res->$id->cargo = $cargoModel->where("activo",1)->find($nombre_data["cargo"])["nombre"];
			$res->$id->tipo = $tipoModel->where("activo",1)->find($nombre_data["tipo"])["nombre"];
			$res->$id->nombre = $nombre;
			$res->$id->asociado = $nombre_data["consejero_asociado"];
		}
		return $res;
		
	}

	public function genAsistencia() {
		$array_json = array();
		$dauaModel = new DAUAModel();
		$informacionModel = new InformacionConsejeroModel();

		$facs = $dauaModel->where("activo",1)->findAll();

		foreach ($facs as $f) {
			$cons = $informacionModel->where("activo",1)->where("daua",1)->findAll();
			foreach ($cons as $c) {
			if ($c["tipo"] == "1") {
				$propietario = 1;
				$resp = 1;
				$asistencia = 1;
			} else {
				$propietario = 0;
				$resp = 0;
				$asistencia = 0;
			}
			//	$array_json[$f["id_daua"]]["nombre"] = $f["nombre"];
				$array_json[$f["id_daua"]][$c["id"]] = [
						'asistencia' => $asistencia,
						'propietario' => $propietario,
						'resp' => $resp,
						'voto' => 1, // 1= A favor, 2= En contra, 3= Abstencion
						'justificante' => '',
					];
			}
		}
		$array_json = json_encode($array_json);
		return $array_json;
	}

	public function setDataAsistencia($id_sesion,$id_daua, $id, $data) {
		$d = $this->where("activo",1)->find($id_sesion)["asistencia"];

		$json = json_decode($d);

		foreach ($data as $key=>$value) {
			$json->$id_daua->$id->$key = $value;	
		}
		
		$json = json_encode($json);

		$data = [
			'asistencia' => $json,
		];
		if ($this->update($id_sesion,$data)) return true;
		else return false;
	}

	public function getListaAsistenciabyUsr($id) {
		return false;
	}
}
