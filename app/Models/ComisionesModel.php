<?php namespace App\Models;

use CodeIgniter\Model;

class ComisionesModel extends Model {
	protected $table = "comisiones";
	protected $primaryKey = "id_comisiones";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','activo'];

}
