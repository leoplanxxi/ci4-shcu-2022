<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\UsuariosConsejerosModel;
use App\Models\InformacionConsejeroModel;
use App\Models\AcuseModel;
use App\Models\DAUAModel;

class SolicitudesModel extends Model {
	protected $table = "solicitudes";
	protected $primaryKey = "id_solicitud";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','descripcion','fecha','archivo','estado','respuesta','detalle_respuesta','acuse_idacuse','enlace','id_usuario','cerrado','activo'];

	public function getSolicitudes($id_usuario=-1) {
		if ($id_usuario == -1) $sols = $this->where("activo",1)->findAll(); #Todas las solicitudes
		else $sols = $this->where("activo",1)->where("id_usuario",$id_usuario)->findAll(); #Por id_usuario

		$usrConsjModel = new UsuariosConsejerosModel();
		$infoConsjModel = new InformacionConsejeroModel();
		$dauaModel = new DAUAModel();
		$acuseModel = new AcuseModel();

		$i=0;
		foreach ($sols as $s) {
			//Estado
			if ($s["estado"] == "0") $sols[$i]["estado"] = "Recibido";
			if ($s["estado"] == "1") $sols[$i]["estado"] = "En seguimiento";
			if ($s["estado"] == "2") $sols[$i]["estado"] = "Respuesta";

			$id_usuario = $s["id_usuario"];
			$s["id_usuario"] = $usrConsjModel->find($id_usuario)["id"];
			$datos = $infoConsjModel->find($s["id_usuario"]);
			$sols[$i]["id_usuario"] = $datos["nombre"] . " " . $datos["apellido_paterno"] . " " . $datos["apellido_materno"];
			$sols[$i]["daua"] = $dauaModel->find($datos["daua"])["nombre"];
			$sols[$i]["acuse_idacuse"] = $acuseModel->find($s["acuse_idacuse"])["nombre"];

			$i++;
		}
		return $sols;
	}

	public function getSolicitudbyID($id_solicitud) {
		$sol = $this->where("activo",1)->find($id_solicitud);

		$usrConsjModel = new UsuariosConsejerosModel();
		$infoConsjModel = new InformacionConsejeroModel();
		$dauaModel = new DAUAModel();
		$acuseModel = new AcuseModel();

		//Estado
		$sol["raw_estado"] = $sol["estado"];
		if ($sol["estado"] == "0") $sol["estado"] = "Recibido";
		if ($sol["estado"] == "1") $sol["estado"] = "En seguimiento";
		if ($sol["estado"] == "2") $sol["estado"] = "Respuesta";

		$id_usuario = $sol["id_usuario"];
		$sol["id_usuario"] = $usrConsjModel->find($id_usuario)["id"];
		$datos = $infoConsjModel->find($sol["id_usuario"]);
		$sol["id_usuario"] = $datos["nombre"] . " " . $datos["apellido_paterno"] . " " . $datos["apellido_materno"];
		$sol["daua"] = $dauaModel->find($datos["daua"])["nombre"];
		$sol["id_acuse"] = $sol["acuse_idacuse"];
		$sol["acuse_idacuse"] = $acuseModel->find($sol["acuse_idacuse"])["nombre"];

		
		return $sol;
		
	}

}
