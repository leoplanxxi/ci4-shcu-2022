<?php namespace App\Models;

use CodeIgniter\Model;

use App\Models\CargoConsejeroModel;
use App\Models\ComisionesModel;
use App\Models\TipoConsejeroModel;
use App\Models\DAUAModel;
use App\Models\UsuariosConsejerosModel;
use App\Models\SesionModel;
use App\Models\SolicitudesModel;

class InformacionConsejeroModel extends Model {
	protected $table = "informacion_consejero";
	protected $primaryKey = "id";

	protected $returnType = 'array';

    protected $useAutoIncrement = false;

	protected $allowedFields = ['id','grado','nombre','apellido_paterno','apellido_materno','cumpleanos','cargo','tipo','daua','tel_cel_1','tel_cel_1_whats','tel_cel_2','tel_cel_2_whats','tel_casa','tel_oficina','extension','correo_institucional','correo_personal','protesta','comision', 'consejero_asociado'];

	private function _group_by($array, $key) {
	    $return = array();
	    foreach($array as $val) {
	        $return[$val[$key]][] = $val;
	    }
	    return $return;
	}

	public function getallConsejeros($groupBy = "") {
		$res = $this->where("activo",1)->findAll();	

		$cargoModel = new CargoConsejeroModel();
		$tipoModel = new TipoConsejeroModel();
		$dauaModel = new DAUAModel();
		$comisionModel = new ComisionesModel();

		$array_salida = array();
		$i=0;

		foreach ($res as $r) {
			$cargo = $cargoModel->find($r["cargo"])["nombre"];
			$tipo = $tipoModel->find($r["tipo"])["nombre"];
			$daua = $dauaModel->find($r["daua"])["nombre"];
			$comision = $comisionModel->find($r["comision"])["nombre"];
			
			$array_salida[$i] = array(
				'id' => $r["id"],
				'nombre' => $r["nombre"] . " " . $r["apellido_paterno"] . " " . $r["apellido_materno"],
				'cargo' => $cargo,
				'tipo' => $tipo,
				'daua' => $daua,
				'comision' => $comision,
				'consejero_asociado' => $r["consejero_asociado"],
			);
			$i++;
		}

		if ($groupBy != "")  $array_salida =$this->_group_by($array_salida,$groupBy);
		return $array_salida;
	}	
	
	public function getConsejerosbyDAUA_grouped($id_daua, $groupBy = "") {
		$res = $this->where("activo",1)->where("daua",$id_daua)->findAll();

		$cargoModel = new CargoConsejeroModel();
		$tipoModel = new TipoConsejeroModel();

		$array_salida = array();
		$i=0;
		foreach ($res as $r) {
			$cargo = $cargoModel->find($r["cargo"])["nombre"];
			$tipo = $tipoModel->find($r["tipo"])["nombre"];
			$array_salida[$i] = array(
				'id' => $r["id"],
				'cargo' => $cargo,
				'tipo' => $tipo,
				'consejero_asociado' => $r["consejero_asociado"],
			);
			$i++;
		}
		if ($groupBy != "") $array_salida =$this->_group_by($array_salida,"cargo");
		return $array_salida;
	}

	public function getExpediente($id) {
		$res = $this->where("activo",1)->find($id);
		
		$cargoModel = new CargoConsejeroModel();
		$tipoModel = new TipoConsejeroModel();
		$comisionModel = new ComisionesModel();
		$sesionModel = new SesionModel();
		$solicitudModel = new SolicitudesModel();
		$usuarioModel = new UsuariosConsejerosModel();
		$dauaModel = new DAUAModel();
		
		$res["cargo"] = $cargoModel->find($res["cargo"])["nombre"];
		$res["tipo"] = $tipoModel->find($res["tipo"])["nombre"];
		$res["comision_id"] = $res["comision"];
		$res["comision"] = $comisionModel->find($res["comision"])["nombre"];
		$res["daua_n"] = $dauaModel->find($res["daua"])["nombre"];

		$id_tbl = $usuarioModel->where("id",$res["id"])->findAll()[0]["id_usuarios_consejeros"];

		$res["solicitudes"] = $solicitudModel->getSolicitudes($id_tbl);

		$sesiones_tbl = $sesionModel->findAll();
		$i=0;
		foreach ($sesiones_tbl as $s) {
			$usr_daua = $res["daua"];
			$res["sesiones"][$i]["sesion"] = $s["nombre"]. " (".$s["fecha"].")";

			$asistencia = json_decode($s["asistencia"]);
			$asistencia = $asistencia->$usr_daua->$id;
			if ($asistencia->asistencia == 1) $res["sesiones"][$i]["asistencia"] = "Asistencia";
			else $res["sesiones"][$i]["asistencia"] = "Falta";

			if ($asistencia->voto == "1") $res["sesiones"][$i]["voto"] = "A favor";
			else if ($asistencia->voto == "2") $res["sesiones"][$i]["voto"] = "En contra";			
			else if ($asistencia->voto == "3") $res["sesiones"][$i]["voto"] = "Abstención";			
			else $res["sesiones"][$i]["voto"] = "Sin voto";

		}
		return $res;
	}
}
