<?php namespace App\Models;

use CodeIgniter\Model;

class ArchivoMaterialSesionModel extends Model {
	protected $table = "archivo_material_sesion";
	protected $primaryKey = "id_archivo_material_sesion";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','descripcion','archivo','fecha_subida','activo','sesion'];

	public function getMaterialbySesion($id_sesion) {
		return $this->where("activo",1)->where("sesion",$id_sesion)->findAll();
	}
}
