<?php namespace App\Models;

use CodeIgniter\Model;

class UsuariosConsejerosModel extends Model {
	protected $table = "usuarios_consejeros";
	protected $primaryKey = "id_usuarios_consejeros";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['id','usuario','passwd','activo','passwd_cambio'];
/*
	public function __construct() {
		parent::__construct();

		$this->db = \Config\Database::connect();
	}

	public function setPass($id,$pass) {
		$pass = password_hash($pass,PASSWORD_BCRYPT);

		$data["passwd"] = $pass;

		$this->db->table($this->table)->update($data,array('id_usuarios_consejeros'=>$id));
		return $this->db->affectedRows();
	}

	public function getPass($id) {
		$query = $this->db->query("SELECT passwd FROM " . $this->table);
		return $query->getResult();
	}*/
}
