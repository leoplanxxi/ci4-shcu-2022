<?php namespace App\Models;

use CodeIgniter\Model;

class TipoConsejeroModel extends Model {
	protected $table = "tipo_consejero";
	protected $primaryKey = "id_tipo";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','activo'];

}
