<?php namespace App\Models;

use CodeIgniter\Model;

class ComisionesConsejerosModel extends Model {
	protected $table = "comisiones";
	/*protected $primaryKey = "";*/

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['id_consejero','di_comision','activo'];

}
