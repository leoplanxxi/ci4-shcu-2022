<?php namespace App\Models;

use CodeIgniter\Model;

class AsistenciaModel extends Model {
	protected $table = "asistencia";
	protected $primaryKey = "id_asistencia";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['id_usuario','id_sesion','asistencia','activo','voto','propietario','id_usuario_suplente','justificante','descripcion_justificante'];

}
