<?php namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Recipe;

class NameModel extends Model {
	protected $table = "";

	protected $returnType = NameEntity::Class;

	protected $allowedFields = ['field_1','field_2',];
}
