<?php namespace App\Models;

use CodeIgniter\Model;

class DAUAModel extends Model {
	protected $table = "daua";
	protected $primaryKey = "id_daua";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','activo'];

}
