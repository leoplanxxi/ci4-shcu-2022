<?php namespace App\Models;

use CodeIgniter\Model;

class UsuariosAdminModel extends Model {
	protected $table = "usuarios_admin";
	protected $primaryKey = "idusuarios_admin";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['rol','usuario','passwd','activo','daua'];

}
