<?php namespace App\Models;

use CodeIgniter\Model;

class CargoConsejeroModel extends Model {
	protected $table = "cargo_consejero";
	protected $primaryKey = "id_cargo_consejero";

	protected $returnType = 'array';

    protected $useAutoIncrement = true;

	protected $allowedFields = ['nombre','activo'];

}
