<?php

namespace App\Libraries;

class libHCU {
	public function menuRol($rol) {
		switch($rol) {
			case '5':
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Sesiones' => base_url("sesiones/lista"),
					'Boletines' => 'https://boletin.buap.mx$$_blank',
					'Solicitudes de Gestión' => base_url("solicitudes/lista"),
					'Cerrar sesión' => base_url("login/logout"),
				);
				break;
			case '3':
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Sesiones' => base_url("sesiones/lista"),
					'Boletines' => 'https://boletin.buap.mx$$_blank',
					'Solicitudes de Gestión' => base_url("solicitudes/lista"),
					'Directorio' => base_url('directorio/lista'),
					'Cerrar sesión' => base_url("login/logout"),
				);
				break;
			case '2':
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Sesiones' => base_url("sesiones/lista"),
					'Boletines' => 'https://boletin.buap.mx$$_blank',
					'Solicitudes de Gestión' => base_url("solicitudes/lista"),
					'Cerrar sesión' => base_url("login/logout"),
				);
				break;
			case '1':
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Sesiones' => base_url("sesiones/lista"),
					'Boletines' => 'https://boletin.buap.mx$$_blank',
					'Solicitudes de Gestión' => base_url("solicitudes/lista"),
					'Aviso de sesión' => '#',
					'Usuarios' => '#',
					'Solicitudes de Gestión' => '#',
					'Cerrar sesión' => base_url("login/logout"),
				);
				break;
			case '4':
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Asistencias' => base_url("sesiones/lista"),
					'Cerrar sesión' => base_url("/login/logout"),
				);
				break;
			default:
				$array_menu = array(
					'Inicio' => base_url('intro'),
					'Cerrar sesión' => base_url("login/logout"),
 				);
 				break;
		}	

		return $array_menu;
	}
}
