<?php

namespace App\Libraries;

class libJWT {
	public function getJWT() {
		if (isset($_SESSION["logged_in"])) {
			$base = config('URLConfig')->getBase();
			$url = $base . "/API/issueKey";
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);
			curl_close($ch);
			return $result;			
		} else { return false; }

		
	}

	public function createResponse($status,$error,$messages,$data) {
		return [
			'status' => $status,
			'error' => $error,
			'messages' => $messages,
			'data' => $data,
		];
	}


	public function putData($arr_config) {
		$base = config('URLConfig')->getBase();
		$url = $base.$arr_config["URI"];

		if ($arr_config["method"] !== "POST") {
			$get_args = http_build_query($arr_config["args"]);
			$url = $url."?".$get_args;
		}
	
		$ch = curl_init($url);

		$auth = array(
			'Authorization: ' . $arr_config["auth"],
/*			'Content-Type:application/json',*/
		);

		if ($arr_config["method"] == "POST") {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arr_config["args"]));	
		} 

		curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		$result = curl_exec($ch);
		curl_close($ch);
	/*	echo "<pre>";
		var_dump($result);*/
		return $result;
	}
}
